<?php
/**
 *template name: Business Page (Test)
 **/
?>
<?php
wp_enqueue_style('slick-css');

//HEADER
get_header();
?>

<?php do_action('get_slider', $post->ID); ?>
	<!--SLIDER-->
    <?php
        $args = array(
            'taxonomy' => 'projects_type',
            'hide_empty' => true,
        );
        $project_type = get_terms($args);
    ?>
    <?php if(!empty($project_type)): ?>
        <section class="bg-light mt-4">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-center">Тип проекта</h1>
                    </div>
                </div>
                <div id="project_categories" class="row justify-content-center">
                    <?php foreach($project_type as $item): ?>
                        <div class="col-md-4 col-lg-4 col-xl-3">
                            <a href="<?php echo get_home_url(). '/projects/?fwp_categories='. $item->slug; ?>" class="home-category mb-1 mt-1 mb-md-2 mt-md-3">
                                    <span class="home-category-item">
                                        <span class="front">
                                            <?php $color = get_field('color', $item); ?>
                                            <?php $color = (!empty($color)) ? $color : '#1d9d73'; ?>
                                            <span class="icon" style="background: <?php echo $color; ?>">
                                                <?php  $icon_url = category_image_src(array('term_id' =>$item->term_id, 'size' => 'full'), false); ?>
                                                <?php if(!empty($icon_url)): ?>
                                                    <img src="<?php echo $icon_url; ?>">
                                                <?php endif; ?>
                                            </span>
                                            <span class="name mt-md-3 pl-3 pl-md-2 pr-2 text-md-center"><?php echo $item->name; ?></span>
                                        </span>
                                        <span class="back" style="background: <?php echo $color; ?>">
                                            <span class="count"><?php echo $item->count. ' Обьявлений'; ?></span>
                                        </span>
                                    </span>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

<?php var_dump( intval(current_time('G')) );?>
<!--	--><?php
//		$args = array(
//		    'taxonomy' => 'projects_cat',
//		    'hide_empty' => true,
//		);
//		$project_cat = get_terms($args);
//	?>
<!---->
<!--	--><?php //if(!empty($project_cat)): ?>
<!--		<section class="bg-light">-->
<!--		    <div class="container">-->
<!--		        <div class="row">-->
<!--		            <div class="col-12">-->
<!--		                <h1 class="text-center">Категории</h1>-->
<!--		            </div>-->
<!--		        </div>-->
<!--			    <div id="project_categories" class="row">-->
<!--				    --><?php //foreach($project_cat as $item): ?>
<!--					    <div class="col-md-4 col-lg-4 col-xl-3">-->
<!--						    <a href="--><?php //echo get_home_url(). '/projects/?fwp_categories='. $item->slug; ?><!--" class="home-category mb-1 mt-1 mb-md-2 mt-md-3">-->
<!--							    <span class="home-category-item">-->
<!--								    <span class="front">-->
<!--									    --><?php //$color = get_field('color', $item); ?>
<!--									    --><?php //$color = (!empty($color)) ? $color : '#1d9d73'; ?>
<!--									    <span class="icon" style="background: --><?php //echo $color; ?><!--">-->
<!--										    --><?php // $icon_url = category_image_src(array('term_id' =>$item->term_id, 'size' => 'full'), false); ?>
<!--										    --><?php //if(!empty($icon_url)): ?>
<!--										        <img src="--><?php //echo $icon_url; ?><!--">-->
<!--						                    --><?php //endif; ?>
<!--									    </span>-->
<!--							            <span class="name mt-md-3 pl-3 pl-md-2 pr-2 text-md-center">--><?php //echo $item->name; ?><!--</span>-->
<!--								    </span>-->
<!--								    <span class="back" style="background: --><?php //echo $color; ?><!--">-->
<!--									    <span class="count">--><?php //echo $item->count. ' Обьявлений'; ?><!--</span>-->
<!--								    </span>-->
<!--							    </span>-->
<!--						    </a>-->
<!--					    </div>-->
<!--					--><?php //endforeach; ?>
<!--			    </div>-->
<!--                <div class="row more-cats mt-4 mb-4">-->
<!--                    <div class="col-12 d-flex justify-content-center">-->
<!--                        <div class="show active">-->
<!--                            <span>Все категории</span>-->
<!--                            <span class="lnr lnr-chevron-down"></span>-->
<!--                        </div>-->
<!--                        <div class="hide">-->
<!--                            <span>Меньше</span>-->
<!--                            <span class="lnr lnr-chevron-up"></span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--		    </div>-->
<!--		</section>-->
<!--	--><?php //endif; ?>
	<!--ENDED PROJECTS-->
<?php //do_action('get_ended_projects'); ?>

	<!--<section class="light">-->
	<!--    <div class="container">-->
	<!--        <div class="row">-->
	<!--            <div class="col-12 text-center pt-5 pb-3">-->
	<!--                <a href="http://antvan.ru">-->
	<!--                    <img src="--><?php //echo get_stylesheet_directory_uri(). '/img/antvan-logo.png'?><!--">-->
	<!--                </a>-->
	<!--            </div>-->
	<!--        </div>-->
	<!--    </div>-->
	<!--</section>-->


<?php if (is_active_sidebar('business_main_widget')): ?>
	<?php dynamic_sidebar( 'business_main_widget'); ?>
<?php endif; ?>


<?php do_action('get-partners-slider'); ?>


<?php
wp_enqueue_script('tween-max-js');
wp_enqueue_script('slick-slider-js');
wp_enqueue_script('scroll-magic-js');
wp_enqueue_script('scroll-magic-animation-js');
wp_enqueue_script('scroll-magic-indicator-js');
wp_enqueue_script('home-js');

//FOOTER
get_footer();
?>