<?php

add_action('before_show_page', function(){
	$action = $_POST['action'];
	if($action == 'update-user-profile-general'){
		$userdata = array(
			'ID'              => wp_get_current_user()->ID,
			'first_name'      => $_POST['first_name'],
			'last_name'       => $_POST['last_name'],
		);
		wp_update_user($userdata);

		update_user_meta(wp_get_current_user()->ID, 'country', $_POST['country']);
		update_user_meta(wp_get_current_user()->ID, 'city', $_POST['city']);
		update_user_meta(wp_get_current_user()->ID, 'phone', $_POST['phone']);

	}
});
add_action('verify_user_permission', function(){

	$site_url = trim($_SERVER['REQUEST_URI'], '/');
	$cur_user_name = wp_get_current_user()->user_login;
	$url_parts = explode('/', $site_url);

	if($url_parts[1] != $cur_user_name){
		wp_safe_redirect( home_url() );
		exit;
	}
});

add_action('init', function () {
	ob_start();
});

add_action('get_user_name', function(){
	$output = '';

	$first_name = wp_get_current_user()->first_name;
	$last_name = wp_get_current_user()->last_name;

	if(!empty($first_name) || !empty($last_name)){
		$output .= $first_name. ' ';
		$output .= $last_name;
	}else{
		$output = wp_get_current_user()->nickname;
	}

	echo $output;
});

add_action('get_user_profile_settings_url', function(){
	echo site_url(). '/profile/'. wp_get_current_user()->user_login. '/settings';
});

add_action('get_user_messages_url', function(){
	echo site_url(). '/profile/'. wp_get_current_user()->user_login. '/messages';
});

add_action('get_user_notifications_url', function(){
	echo site_url(). '/profile/'. wp_get_current_user()->user_login. '/notifications';
});

add_filter('prepare_message_subject', function($subject){
	echo preg_replace('/Re: /', '', $subject);
});


function get_threads_between_users( $user_id, $other_id ) {

	if ( ! function_exists('bp_is_active') || ! bp_is_active( 'messages' ) ) {
		return array();
	}

	$bp = buddypress();
	global $wpdb;

	$table_messages = $bp->messages->table_name_messages;
	$table_recipients = $bp->messages->table_name_recipients;

	$sql = $wpdb->prepare( "SELECT thread_id FROM {$table_messages} WHERE sender_id = %d", $user_id );

	$sql .= $wpdb->prepare( " AND thread_id IN ( SELECT thread_id FROM {$table_recipients} WHERE user_id = %d )", $other_id );

	$sent_threads = $wpdb->get_col( $sql );

	$sql = $wpdb->prepare( "SELECT thread_id FROM {$table_messages} WHERE sender_id = %d", $other_id );

	$sql .= $wpdb->prepare( " AND thread_id IN ( SELECT thread_id FROM {$table_recipients} WHERE user_id = %d )", $user_id );

	$threads_by_other_user = $wpdb->get_col( $sql );

	$thread_ids = array_unique( array_merge( $sent_threads, $threads_by_other_user ) );

	return $thread_ids;
}
