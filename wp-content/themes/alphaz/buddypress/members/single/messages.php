<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <ul class="sidebar-nav mt-lg-3">
                <li class="mr-2 mr-lg-0">
                    <a href="<?php echo site_url(). '/profile/'. wp_get_current_user()->user_login. '/messages'; ?>" class="tab-item <?php echo (bp_current_action() == 'inbox') ? 'active' : ''?>">Входящие</a>
                </li>
                <li class="mr-2 mr-lg-0">
                    <a href="<?php echo site_url(). '/profile/'. wp_get_current_user()->user_login. '/messages/sentbox'; ?>" class="tab-item <?php echo (bp_current_action() == 'sentbox') ? 'active' : ''?>">Исходящие</a>
                </li>
                <li class="mr-2 mr-lg-0 d-none d-sm-inline-block">
                    <a href="<?php echo site_url(). '/profile/'. wp_get_current_user()->user_login. '/messages/starred'; ?>" class="tab-item <?php echo (bp_current_action() == 'starred') ? 'active' : ''?>">Помеченые</a>
                </li>
            </ul>
        </div>
        <div id="messages" class="col-lg-10 mt-3">
            <?php
                if(bp_current_action() == 'inbox'){
                    bp_get_template_part( 'members/single/messages/messages-loop' );
                }else if(bp_current_action() == 'sentbox'){
	                bp_get_template_part( 'members/single/messages/messages-loop' );
                }else if(bp_current_action() == 'starred'){
	                bp_get_template_part( 'members/single/plugins' );
                }else if(bp_current_action() == 'view'){
	                bp_get_template_part( 'members/single/messages/single' );
                }
            ?>
        </div>
    </div>
</div>