<?php do_action('before_show_page'); ?>
<form action="" method="post">
    <input type="hidden" name="action" value="update-notification-settings">
    <div class="row">
        <div class="col">
            <span>Email уведомления</span>
            <div class="radio-field">
                <label for="">Присылать уведомления о новом сообщении</label>
                <input type="radio" value="yes" checked>
            </div>
            <div class="radio-field">
                <label for="">Отключить</label>
                <input type="radio" value="no">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col d-flex justify-content-center">
            <input type="submit" value="Сохранить" class="btn-filled">
        </div>
    </div>
</form>