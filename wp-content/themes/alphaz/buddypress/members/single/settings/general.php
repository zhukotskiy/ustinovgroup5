<?php do_action('before_show_page'); ?>
<form action="" method="post">
    <input type="hidden" name="action" value="update-user-profile-general">
    <div class="row">
        <div class="col-md-6">
            <div class="input-field">
                <label for="first_name">Имя</label>
                <input type="text" name="first_name" id="first_name" placeholder="Иван" value="<?php echo wp_get_current_user()->first_name; ?>" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-field">
                <label for="last_name">Фамилия</label>
                <input type="text" name="last_name" id="last_name" placeholder="Иванов" value="<?php echo wp_get_current_user()->last_name; ?>" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-field">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" placeholder="example@mail.com" value="<?php echo wp_get_current_user()->user_email; ?>" disabled>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="input-field">
                <label for="country">Страна</label>
                <input type="text" name="country" id="country" placeholder="Украина" value="<?php echo get_user_meta(wp_get_current_user()->ID, 'country', true) ?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-field">
                <label for="city">Город</label>
                <input type="text" name="city" id="city" placeholder="Киев" value="<?php echo get_user_meta(wp_get_current_user()->ID, 'city', true) ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="input-field">
                <label for="phone">Телефон</label>
                <input type="text" name="phone" id="phone" placeholder="+38 (000) 000 0000" value="<?php echo get_user_meta(wp_get_current_user()->ID, 'phone', true) ?>">
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 d-flex justify-content-end">
            <input type="submit" class="filled-btn" value="Сохранить">
        </div>
    </div>
</form>