<?php do_action('verify_user_permission'); ?>
<div id="profile-page">
    <section class="light pt-5 pb-0 pb-lg-3" >
        <div class="container">
            <div class="row">
                <div class="col d-none d-sm-block">
                    <h5 class="text-right font-type-2">Рады снова вас видеть, <?php do_action('get_user_name'); ?></h5>
                </div>
                <div class="col d-block d-sm-none">
                    <h5 class="text-right font-type-2">Рады снова вас видеть,<br><?php do_action('get_user_name'); ?></h5>
                </div>
            </div>
            <div class="row">
                <div class="col"><h3>Профиль</h3></div>
<!--                <div class="col">-->
<!--                    <h3 class="text-right font-type-2 mb-0">$600</h3>-->
<!--                    <h6 class="text-right text-caption pt-0 mt-0 font-type-2">на счету</h6>-->
<!--                </div>-->
            </div>
            <div class="row">
                <div class="col-12 navigation d-flex flex-wrap">
                    <a href="<?php echo site_url().'/profile/user-projects'?>" class="tab-item">
                        <span>Мои проекты</span>
                    </a>
                    <a href="<?php do_action('get_user_messages_url') ?>" class="tab-item <?php echo (bp_is_user_messages()) ? 'active' : '' ;?>">
                        <span>Сообщения</span>
                    </a>
                    <a href="<?php echo site_url().'/profile/favorite'?>" class="tab-item">
                        <span>Избранное</span>
                    </a>
                    <a href="<?php do_action('get_user_profile_settings_url') ?>" class="tab-item <?php echo (bp_is_user_settings()) ? 'active' : '' ;?>">
                        <span>Настройки аккаунта</span>
                    </a>

                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="divider"></div>
            </div>
        </div>
    </section>
    <section class="light pt-0 pt-lg-3">
        <?php
            if(bp_is_user_settings()){
                bp_get_template_part( 'members/single/settings' );
            }else if(bp_is_user_messages()){
                bp_get_template_part( 'members/single/messages' );
            }else if(bp_is_user_notifications()){
                bp_get_template_part( 'members/single/notifications' );
            }
        ?>
    </section>
</div>