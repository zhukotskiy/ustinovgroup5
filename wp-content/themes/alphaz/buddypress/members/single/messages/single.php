<?php do_action( 'bp_before_message_thread_content' ); ?>
<div id="single-message">
	<?php if (bp_thread_has_messages()) : ?>
		<h3 class="text-center mt-0 pt-0"><?php apply_filters('prepare_message_subject', bp_get_the_thread_subject()); ?></h3>

		<?php while ( bp_thread_messages() ) : bp_thread_the_message(); ?>
			<?php bp_get_template_part( 'members/single/messages/message' ); ?>
		<?php endwhile; ?>

		<?php do_action( 'bp_after_message_thread_list' ); ?>
		<?php do_action( 'bp_before_message_thread_reply' ); ?>

		<form id="send-reply" action="<?php bp_messages_form_action(); ?>" method="post" class="standard-form">
			<div class="row mt-3 pt-3">

                <?php do_action( 'bp_before_message_reply_box' ); ?>

                <div class="col-md-11 pt-3 pb-3">
                    <input type="hidden" id="message_content" name="content" data-type="materialTextArea" data-placeholder="Написать ответ">
                </div>

                <?php do_action( 'bp_after_message_reply_box' ); ?>

                <div class="col-md-1 pt-3 pb-3 d-flex justify-content-end">
                    <button id="send_reply_button">
                        <i class="material-icons shortcut-icon simple">send</i>
                    </button>
                </div>

                <input type="hidden" id="thread_id" name="thread_id" value="<?php bp_the_thread_id(); ?>" />
                <input type="hidden" id="messages_order" name="messages_order" value="<?php bp_thread_messages_order(); ?>" />
                <?php wp_nonce_field( 'messages_send_message', 'send_message_nonce' ); ?>
            </div>
		</form>

		<?php do_action( 'bp_after_message_thread_reply' ); ?>
	<?php endif; ?>
	<?php do_action( 'bp_after_message_thread_content' ); ?>
</div>
