<?php
    global $thread_template;
    $user = get_user_by('id', $thread_template->message->sender_id);
    $messageCss = ($user->ID == wp_get_current_user()->ID) ? 'sent-by-me' : '';
?>
<div class="message <?php echo $messageCss; ?>">
    <div class="message-item">
        <?php do_action( 'bp_before_message_content' ); ?>
        <div class="metadata">
            <?php
                $user_name = (!empty($user->last_name) && !empty($user->first_name)) ? $user->last_name. ' '. $user->first_name : $user->nickname;
            ?>
            <span><?php echo $user_name; ?></span>
            <span><?php echo bp_core_time_since( bp_get_the_thread_message_date_sent() ) ?></span>
        </div>
        <div class="content">
            <?php bp_the_thread_message_content(); ?>
        </div>
        <?php do_action( 'bp_after_message_content' ); ?>
    </div>
</div>
