<?php do_action( 'bp_before_member_messages_loop' ); ?>
<?php if (bp_has_message_threads(bp_ajax_querystring('messages'))): ?>
	<form action="<?php echo bp_displayed_user_domain() . bp_get_messages_slug() . '/' . bp_current_action() ?>/bulk-manage/" method="post" id="messages-bulk-management">
	    <?php while (bp_message_threads()) : bp_message_thread(); ?>
            <div class="row  message-link pb-3 pt-3">
                <div class="align-items-center favorite-link d-none d-sm-flex">
	                <?php if (bp_is_active( 'messages', 'star' )): ?>
                        <span class="favorite">
                            <?php bp_the_message_star_action_link(array('thread_id' => bp_get_message_thread_id())); ?>
                        </span>
	                <?php endif; ?>
                </div>
                <a href="<?php bp_message_thread_view_link(bp_get_message_thread_id(), bp_displayed_user_id()); ?>" class="col">
                    <div class="row">
                        <div class="col-sm-7 col-md-6">
                            <h3 class="m-0 p-0"><?php apply_filters('prepare_message_subject', bp_get_message_thread_subject()); ?></h3>
                            <p class="m-0 p-0 text-secondary"><?php bp_message_thread_excerpt(); ?></p>
                        </div>
                        <div class="col-sm-5 col-md-3">
                            <?php
                                global $messages_template;
                                $user = get_user_by('id', $messages_template->thread->last_sender_id);
                                $user_name = (!empty($user->last_name) && !empty($user->first_name)) ? $user->last_name. ' '. $user->first_name : $user->nickname;
                            ?>
                            <h5 class="sender"><?php echo $user_name; ?></h5>
                        </div>
                        <div class="col-md-3 d-none d-md-block">
                            <h5 class="last-activity"><?php bp_message_thread_last_post_date(); ?></h5>
                        </div>
                    </div>
                </a>
                <div class="col-4 d-flex col-md-1 justify-content-end align-items-center">
                    <a class="delete" href="<?php bp_message_thread_delete_link( bp_displayed_user_id() ); ?>"><i class="shortcut-icon material-icons delete">delete</i></a>
	                <?php do_action( 'bp_messages_thread_options' ); ?>
                </div>
            </div>
        <?php endwhile; ?>
	</form>
<?php else: ?>
	<h3 class="text-center text-secondary">Сообщений не найдено</h3>
<?php endif;?>
<?php do_action( 'bp_after_member_messages_loop' ); ?>
