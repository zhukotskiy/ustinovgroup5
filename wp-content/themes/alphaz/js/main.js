$ = jQuery;

$('#mobile-menu').click(function(){
    var self = $(this);
    var headerHeight = $('#header').height() + 1;
    var menuSection = $(this).find('.menu-section');
    var body = $('body');

    //self.toggleClass('is-active');

    if(self.hasClass('is-active')){
        self.removeClass('is-active');
        body.css({'height': 'auto', 'overflow': 'auto'});
    }else{
        body.css({'height': '100vh', 'overflow': 'hidden'});
        menuSection.css({
            'height': 'calc(100vh - ' + headerHeight + 'px)',
            'top': headerHeight + 'px'
        });
        self.addClass('is-active');
    }
});

var modals = {
    el: $('[data-modal_link]'),
    init: function(){
        var self = this;
        self.el.each(function(){
            var cur_elem = $(this);
            var cur_modal = $('[data-modal_window="' + cur_elem.data('modal_link') + '"]');

            cur_elem.click(function(){
                if(cur_modal.hasClass('show')){
                    self.close_modal(cur_modal);
                }else{
                    self.open_modal(cur_modal);
                }
            });

            cur_modal.find('.close-modal-window')
                .click(function(){
                    self.close_modal(cur_modal);
                });
        })
    },
    open_modal: function(modal){
        $('body').css({'height': '100vh', 'overflow': 'hidden'});
        modal.css({'display': 'flex'});
        setTimeout(function(){
            modal.addClass('show');
        }, 50);
    },
    close_modal: function(modal){
        modal.removeClass('show');
        setTimeout(function(){
            modal.css({'display': 'none'});
            $('body').css({'height': 'auto', 'overflow': 'auto'});
        }, 400);
    }
};

var headerNav = {
    parentLink: $('.header-navigation .has-submenu'),
    initSubMenu: function(){
        var self = this;
        self.parentLink.each(function(){
            $(this).hover(function(){
                var self = $(this);

                self.find('ul.submenu')
                    .css({'display': 'flex'});
                setTimeout(function(){
                    self.addClass('show');
                }, 50);

            },function(){
                var self = $(this);

                self.removeClass('show');

                setTimeout(function(){
                    self.find('ul.submenu')
                        .css({'display': 'none'});
                }, 300);

            })
        });
    }
};

var homePage = {
    header: {
        el: $('#header'),
        height: 0
    },
    adminBar: {
        el: $('#wpadminbar'),
        height: 0
    },
    homeSlider: {
        el: $('#home-slider')
    },
    setHomeSliderHeight: function(){
        var self = this;

        self.header.height = self.header.el.height();
        self.adminBar.height = (self.adminBar.el.height()) ? self.adminBar.el.height() : 0;

        //self.homeSlider.el.css({'height': 'calc(100vh - ' + (self.header.height + self.adminBar.height) + 'px)'});
    },
    stickHeader: function(){
        var self = this;

        $(window).scroll(function(){
            if ($(window).scrollTop() >= 80) {
                self.header.el.addClass('sticky');
            }
            else {
                self.header.el.removeClass('sticky');
            }
        });
    },
    init: function(){
        var self = this;

        self.setHomeSliderHeight();
        self.stickHeader();
    }
};


$(document).ready(function(){


    homePage.init();

    headerNav.initSubMenu();

    modals.init();


    // $('#header').sticky({
    //     topSpacing: (adminBarHeight) ? adminBarHeight : 0,
    //     zIndex: 999
    // });

    //project page
    var projectContacts = $('#single-project-page .show-contacts');
    if(projectContacts.length){
        $(document).on('click', '#single-project-page .show-contacts', function(){
            if(!$(this).hasClass('disabled')) {
                $(this).addClass('disabled');
                $('#single-project-page #contacts-info').fadeIn();

                var cur_project_id = $(this).data('project_id');

                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    async: true,
                    dataType: 'json',
                    data: {
                        'action': 'set_phone_view_count',
                        'project_id': cur_project_id
                    }
                });
            }

        });
    }
    var projectPageSlider = $('#project-page-slide');
    var projectPageSlideNav = $('#project-page-slide-nav');
    if(projectPageSlider.find('a').length > 0) {
        projectPageSlider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<div class="prev-slide"><span class="lnr lnr-chevron-left"></span></div>',
            nextArrow: '<div class="next-slide"><span class="lnr lnr-chevron-right"></span></div>',
            infinite: false,
            draggable: false,
            asNavFor: '#project-page-slide-nav',
            fade: true
        });
        projectPageSlideNav.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '#project-page-slide',
            dots: false,
            infinite: false,
            focusOnSelect: true,
            prevArrow: false,
            nextArrow: false,
            draggable: false,
            responsive: [
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 350,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });
    }

    projectPageSlider.on('onAfterAppendSubHtml.lg',function(event){
        var header = $('#header').height();
        $('.image-box').css({'top' : header + 'px', 'height': 'calc(100% - ' + header +'px)'});
    });

    if(projectPageSlider.length){
        projectPageSlider.lightGallery({
            addClass: 'image-box',
            share: false,
            hash: false,
            boolean: false,
            fullScreen: false,
            autoplay: false,
            download: false,
            actualSize: false,
            autoplayControls: false,
            selector: 'a'
        });
        projectPageSlider.on('onAfterAppendSubHtml.lg',function(event){
            var adminBarHeight = $('#wpadminbar').height();
            $('.image-box').css({'top' : adminBarHeight + 'px', 'height': 'calc(100% - ' + adminBarHeight +'px)'});
        });

    }
});

//show/hide mobile menu
// $(document).on('click', '#menu', function(){
//     if($(this).hasClass('is-active')){
//         $(this).removeClass('is-active');
//         $('#left-navigation').css({'left': '-300px'});
//         $('header').removeClass('left-menu-show')
//     }else{
//         $('#left-navigation').css({'left': '0px'});
//         $(this).addClass('is-active');
//         $('header').addClass('left-menu-show');
//
//     }
// });

(function() {
    $.fn.materialTextArea = function(){
        var $textAreas = this;
        return $textAreas.each(function(){
            var $this = $(this);
            var $inputWrapper = $('<div>', {
                'class': 'material-textarea-field',
                contenteditable: 'true',
                text: $this.data('placeholder')
            });

            $this.after($inputWrapper);
            $inputWrapper.keyup(function(){
                $this.val($(this).text());
            });
            $inputWrapper.focus(function(){
                if($inputWrapper.text() === $this.data('placeholder')) {
                    $inputWrapper.text('');
                }
            });
            $inputWrapper.focusout(function(){
                if($inputWrapper.text().length === 0){
                    $inputWrapper.text($this.data('placeholder'));
                }
            });

            $(document).ajaxComplete(function() {
                $inputWrapper.text('');
                $this.val('');
            });
        });
    };

    $('[data-type=materialTextArea]').materialTextArea();
}).call(this);


// (function() {
//     $.fn.roTextarea = function(v) {
//         var $textareas;
//         $textareas = this;
//         return $textareas.each(function() {
//             var $ep, $h, $ta, $this, $wr;
//             $this = $(this);
//             $wr = $('<div>', {
//                 'class': 'ro-input-wrapper'
//             });
//             $h = $('<span>', {
//                 'class': 'ro-input-header',
//                 text: $this.attr('data-header')
//             });
//             $ta = $('<div>', {
//                 'class': 'ro-textarea',
//                 contenteditable: 'true'
//             });
//             $wr.append($h);
//             $wr.append($ta);
//             $this.after($wr);
//             $wr.append($this.detach());
//             $ta.focus(function() {
//                 return $wr.toggleClass('active', true);
//             }).focusout(function() {
//                 return $wr.toggleClass('active', false);
//             });
//             $h.click(function() {
//                 if (!$wr.is('.active')) {
//                     return $ta.focus();
//                 }
//             });
//             if ($this.attr('data-controls')) {
//                 $ep = $('<div>', {
//                     'class': 'ro-textarea-controls'
//                 });
//                 $wr.append($ep);
//                 return $(['Bold', 'Italic', 'Strikethrough']).each(function() {
//                     var aType;
//                     aType = this;
//                     return $ep.append($('<button>', {
//                         'class': 'fa fa-' + aType.toLowerCase()
//                     }).click(function(e) {
//                         e.preventDefault();
//                         e.stopPropagation();
//                         (function(t) {
//                             console.log(t);
//                             return document.execCommand(t, null, false);
//                         })(aType);
//                         return false;
//                     }));
//                 });
//             }
//         });
//     };
//
//     $('[data-type=textarea]').roTextarea();
//
// }).call(this);

var userEditProject = {
    projectEditor: $('#edit-project'),
    projectId: '',
    thumbnailEditor: {
        el: '',
        items: '',
        inputFileSelector: $('#thumbnail_new_item'),
        uploadProcess: false
    },
    galleryEditor: {
        el: '',
        items: '',
        inputFileSelector: $('#gallery_new_item'),
        uploadProcess: false
    },
    imgUploader: {
        el: '',
        file: ''
    },
    init: function(){
        if(this.projectEditor.length){
            this.projectId = this.projectEditor.find('input[name="post_id"]')
                .val();

            this.thumbnailEditor.el = $('#thumbnail-setup');
            this.thumbnailSetupInit();

            this.galleryEditor.el = $('#gallery-setup');
            this.gallerySetupInit();
        }
    },
    thumbnailSetupInit: function(){
        var self = this;

        self.thumbnailEditor.items = self.thumbnailEditor.el.find('.file-item');
        self.thumbnailEditor.items.find('i.remove-item')
            .click(function () {
                self.removeProjectThumbnail();
            });

        self.thumbnailEditor.inputFileSelector.change(function() {
            self.setProjectThumbnail();
        });

    },
    gallerySetupInit: function(){
        var self = this;

        self.galleryEditor.items = self.galleryEditor.el.find('.file-item');
        self.galleryEditor.items.each(function(){
            var currentItem = $(this);
            currentItem.find('i.remove-item')
                .click(function(){
                   self.removeImgFromGallery(currentItem);
                });
        });

        self.galleryEditor.inputFileSelector.change(function(){
            self.addImgToGallery();
        });
    },
    removeImgFromGallery: function(item){
        var self = this;

        self.galleryEditor.items.splice($.inArray(item, self.galleryEditor.items), 1);
        item.css({'opacity': '0.5', 'transition': 'all 0.4s'});

        var gallery = $('#gallery');
        var gallery_items = JSON.parse(gallery.val());
        console.log(gallery_items.indexOf(item.data('img_id')));
        gallery_items.splice(gallery_items.indexOf(item.data('img_id')), 1);

        new_gallery_items = JSON.stringify(gallery_items);
        gallery.val(new_gallery_items);
        item.remove();


        // $.ajax({
        //     type: 'POST',
        //     url: ajaxurl,
        //     async: true,
        //     dataType: 'json',
        //     data: {
        //         'action': 'remove_img_from_project_gallery',
        //         'post_id': self.projectId,
        //         'img_id': item.data('img_id')
        //     }
        // }).done(function(response){
        //     if(response.status === 'ok'){
        //         item.remove();
        //     }else{
        //         item.css({'opacity': '1', 'transition': 'all 0.4s'});
        //     }
        // });

    },
    addImgToGallery: function(){
        var self = this;
        var newGalleryItem;

        self.uploadNewImg(self.galleryEditor.inputFileSelector, function (response) {
            if (response.status === 'ok') {
                newGalleryItem.data('img_id', response.img_id);
                newGalleryItem.find('.file-item-prev')
                    .css({'backgroundImage': 'url(' + response.img_url + ')'});
                newGalleryItem.find('.remove-item').fadeIn();
                newGalleryItem.find('.upload-loader')
                    .fadeOut();
                setTimeout(function () {
                    newGalleryItem.find('.upload-loader')
                        .remove();
                }, 200);
                newGalleryItem.css({'opacity': '1'});

                // $.ajax({
                //     type: 'POST',
                //     url: ajaxurl,
                //     async: true,
                //     dataType: 'json',
                //     data: {
                //         'action': 'add_img_to_gallery',
                //         'post_id': self.projectId,
                //         'img_id': response.img_id
                //     }
                // });
                var gallery_items = [];
                var gallery = $('#gallery');

                if(gallery.val().length > 0){
                    gallery_items = JSON.parse(gallery.val());
                }

                gallery_items.push(response.img_id);
                var new_gallery_items = JSON.stringify(gallery_items);
                gallery.val(new_gallery_items);

            }
        });

        newGalleryItem = $('<div></div>', {
            'class': 'col-md-3 file-item pb-4'
        });
        var fileItemPrev = $('<div></div>', {
            'class': 'file-item-prev'
        });
        var removeItem = $('<i></i>', {
            'class': 'material-icons shortcut-icon delete remove-item',
            'text': 'delete'
        })
            .css({'display': 'none'})
            .click(function () {
                self.removeImgFromGallery()
            });
        var loaderItem = $('<div></div>', {
            'class': 'upload-loader'
        });

        fileItemPrev.append(removeItem);
        fileItemPrev.append(loaderItem);
        newGalleryItem.append(fileItemPrev);

        self.galleryEditor.el.find('.add-file')
            .before(newGalleryItem);

        self.thumbnailEditor.items.push(newGalleryItem);
        newGalleryItem.css({'opacity': '0.5', 'transition': 'all 0.4s'})
    },
    removeProjectThumbnail: function(){
        var self = this;
        var currentItem = self.thumbnailEditor.items;
        self.thumbnailEditor.items = '';

        currentItem.css({'opacity': '0.5', 'transition': 'all 0.4s'});

        currentItem.remove();
        $('#thumbnail').val('');

        // $.ajax({
        //     type: 'POST',
        //     url: ajaxurl,
        //     async: true,
        //     dataType: 'json',
        //     data: {
        //         'action': 'remove_post_thumbnail',
        //         'post_id': self.projectId,
        //         'img_id': currentItem.data('img_id')
        //     }
        // }).done(function(response){
        //     if(response.status === 'ok'){
        //         currentItem.remove();
        //         $('#thumbnail').val('');
        //     }else{
        //         currentItem.css({'opacity': '1', 'transition': 'all 0.4s'});
        //     }
        // });
    },
    setProjectThumbnail: function(){
        var self = this;

        if(!self.thumbnailEditor.uploadProcess){
            self.thumbnailEditor.uploadProcess = true;


            if (self.thumbnailEditor.items.length > 0) {
                self.removeProjectThumbnail();
            }

            self.uploadNewImg(self.thumbnailEditor.inputFileSelector, function (response) {
                if (response.status === 'ok') {
                    self.thumbnailEditor.items.data('img_id', response.img_id);
                    self.thumbnailEditor.items.find('.file-item-prev')
                        .css({'backgroundImage': 'url(' + response.img_url + ')'});
                    self.thumbnailEditor.items.find('.remove-item').fadeIn();
                    self.thumbnailEditor.items.find('.upload-loader')
                        .fadeOut();
                    setTimeout(function () {
                        self.thumbnailEditor.items.find('.upload-loader')
                            .remove();
                    }, 200);
                    self.thumbnailEditor.items.css({'opacity': '1'});

                    $('#thumbnail').val(response.img_id);

                    self.thumbnailEditor.uploadProcess = false;

                    // $.ajax({
                    //     type: 'POST',
                    //     url: ajaxurl,
                    //     async: true,
                    //     dataType: 'json',
                    //     data: {
                    //         'action': 'set_post_thumbnail',
                    //         'post_id': self.projectId,
                    //         'img_id': response.img_id
                    //     }
                    // }).done(function (response) {
                    //     if (response.status === 'ok') {
                    //         self.thumbnailEditor.uploadProcess = false;
                    //         console.log(self.thumbnailEditor.uploadProcess);
                    //     }
                    // });

                }
            });

            var fileItem = $('<div></div>', {
                'class': 'col-md-3 file-item pb-4'
            });
            var fileItemPrev = $('<div></div>', {
                'class': 'file-item-prev'
            });
            var removeItem = $('<i></i>', {
                'class': 'material-icons shortcut-icon delete remove-item',
                'text': 'delete'
            })
                .css({'display': 'none'})
                .click(function () {
                    self.removeProjectThumbnail();
                });
            var loaderItem = $('<div></div>', {
                'class': 'upload-loader'
            });

            fileItemPrev.append(removeItem);
            fileItemPrev.append(loaderItem);
            fileItem.append(fileItemPrev);

            self.thumbnailEditor.el.find('.add-file')
                .before(fileItem);

            self.thumbnailEditor.items = fileItem;
            self.thumbnailEditor.items.css({'opacity': '0.5', 'transition': 'all 0.4s'})
        }
    },
    uploadNewImg: function(selector, callback){
        var self = this;

        self.imgUploader.el = selector;

        var formData = new FormData;
        formData.append('action', 'upload_new_image_file');
        formData.append('post_id', self.projectId);
        formData.append('image_file', self.imgUploader.el.prop('files')[0]);

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            async: true,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData
        }).done(function(response){
            callback(response);
        });
    }

};

userEditProject.init();

$(document).on('click', '.more-cats', function(){
    var project_cats = $('#project_categories');
    var self = $(this);

    if(project_cats.hasClass('show')){
        project_cats.removeClass('show');
        self.find('.show')
            .addClass('active');
        self.find('.hide')
            .removeClass('active');

    }else{
        project_cats.addClass('show');
        self.find('.show')
            .removeClass('active');
        self.find('.hide')
            .addClass('active');
    }
});


