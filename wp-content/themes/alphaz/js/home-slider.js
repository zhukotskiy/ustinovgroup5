var homeSlider = {
    TIMER: 0,
    slideInterval: 5000,
    slideComplete: true,
    slideIndex: 0,
    slideSelector: $('#home-slider .slides .item'),
    slideHeight: '',
    titleFontHeight: '',
    initSlider: false,
    init: function(){
        this.slideIndex = 0;
        this.slideHeight = this.slideSelector.height();
        this.slideSelector.eq(this.slideIndex).css({top: 0});
        var titlesList = this.slideSelector.find('.title');
        this.titleFontHeight = titlesList.height();
        titlesList.each(function () {
            var textField = $(this);
            var splitTitle = textField.text()
                .split(' ');
            textField.empty();
            $.each(splitTitle, function (index, item) {
                textField.append('<span class="mask"><span class="text">' + item + '</span></span>');
            });

        });

        for (var i = 0; i < this.slideSelector.length; i++) {
            if (i === 0) {
                $('#home-slider .slides-nav').append('<a href="#" class="active" data-slide="' + i + '"><span></span></a>');
            } else {
                $('#home-slider .slides-nav').append('<a href="#" data-slide="' + i + '"><span></span></a>');
            }
        }

        this.startTimer();
    },
    nextSlide: function(){
        if(this.slideComplete){
            this.slideComplete = false;
            this.stopTimer();

            var currentSlide = this.slideSelector.eq(this.slideIndex);
            var currentSlideImg = currentSlide.find('.item-img');
            var currentSlideImgSrc = currentSlide.find('.item-img-src');
            var currentTitle = currentSlide.find('.title .mask .text');
            var currentLink = currentSlide.find('.link');
            var currentActionLinks = currentSlide.find('.action-links');

            if (this.slideIndex < this.slideSelector.length - 1) {
                this.slideIndex++;
            } else {
                this.slideIndex = 0;
            }

            var nextSlide = this.slideSelector.eq(this.slideIndex);
            var nextSlideImg = nextSlide.find('.item-img');
            var nextSlideImgSrc = nextSlide.find('.item-img-src');
            var nextTitle = nextSlide.find('.title .mask .text');
            var nextLink = nextSlide.find('.link');
            var nextActionLinks = nextSlide.find('.action-links');

            this.slideSelector.eq(this.slideIndex).css({top: this.slideHeight});

            var changeSlide = new TimelineLite();
            changeSlide.fromTo(currentSlideImg, 0.6, {scale: 1}, {scale: 0.5})
                .fromTo(currentSlideImgSrc, 0.6, {scale: 1}, {scale: 1.5}, '-=0.6')
                .fromTo(currentActionLinks, 0.3, {opacity: 1}, {opacity: 0}, '-=0.6')
                .fromTo(currentLink, 0.2, {opacity: 1}, {opacity: 0})
                .fromTo(currentTitle, 0.6, {y: '0px'}, {y: '-' + this.titleFontHeight + 'px'}, '-=0.2')
                .to(currentSlide, 0.7, {top: -this.slideHeight, ease: Power1.easeInOut}, '-=0.6')
                .to(nextSlide, 0.7, {top: '0px', ease: Power1.easeInOut}, '-=0.7')
                .fromTo(nextTitle, 0.6, {y: this.titleFontHeight + 'px'}, {y: '0px'}, '-=0.3')
                .fromTo(nextLink, 0.4, {opacity: 0}, {opacity: 1}, '-=0.4')
                .fromTo(nextSlideImgSrc, 0.6, {scale: 1.5}, {scale: 1}, '-=0.1')
                .fromTo(nextSlideImg, 0.6, {scale: 0.5}, {scale: 1}, '-=0.6')
                .fromTo(nextActionLinks, 0.3, {y: -25, opacity: 0}, {y: 0, opacity: 1});

            changeSlide.eventCallback('onComplete', function () {
                this.slideComplete = true;
                this.startTimer();
            }.bind(this));

            $('.slides-nav a.active').removeClass('active');
            $('.slides-nav a').eq(this.slideIndex).addClass('active');
        }
    },
    prevSlide: function(){
        if(this.slideComplete){
            this.slideComplete = false;
            this.stopTimer();

            var currentSlide = this.slideSelector.eq(this.slideIndex);
            var currentSlideImg = currentSlide.find('.item-img');
            var currentSlideImgSrc = currentSlide.find('.item-img-src');
            var currentTitle = currentSlide.find('.title .mask .text');
            var currentLink = currentSlide.find('.link');
            var currentActionLinks = currentSlide.find('.action-links');

            if (this.slideIndex > 0) {
                this.slideIndex--;
            } else {
                this.slideIndex = this.slideSelector.last().index();
            }

            var nextSlide = this.slideSelector.eq(this.slideIndex);
            var nextSlideImg = nextSlide.find('.item-img');
            var nextSlideImgSrc = nextSlide.find('.item-img-src');
            var nextTitle = nextSlide.find('.title .mask .text');
            var nextLink = nextSlide.find('.link');
            var nextActionLinks = nextSlide.find('.action-links');

            this.slideSelector.eq(this.slideIndex).css({top: -this.slideHeight});

            var changeSlide = new TimelineLite();
            changeSlide.fromTo(currentSlideImg, 0.6, {scale: 1}, {scale: 0.5})
                .fromTo(currentSlideImgSrc, 0.6, {scale: 1}, {scale: 1.5}, '-=0.6')
                .fromTo(currentActionLinks, 0.6, {opacity: 1}, {opacity: 0}, '-=0.6')
                .fromTo(currentLink, 0.2, {opacity: 1}, {opacity: 0})
                .fromTo(currentTitle, 0.6, {y: '0px'}, {y: this.titleFontHeight + 'px'}, '-=0.2')
                .to(currentSlide, 0.7, {top: this.slideHeight, ease: Power1.easeInOut}, '-=0.6')
                .to(nextSlide, 0.7, {top: '0px', ease: Power1.easeInOut}, '-=0.7')
                .fromTo(nextTitle, 0.6, {y: '-' + this.titleFontHeight + 'px'}, {y: '0px'}, '-=0.3')
                .fromTo(nextLink, 0.4, {opacity: 0}, {opacity: 1}, '-=0.4')
                .fromTo(nextSlideImgSrc, 0.6, {scale: 1.5}, {scale: 1}, '-=0.1')
                .fromTo(nextSlideImg, 0.6, {scale: 0.5}, {scale: 1}, '-=0.6')
                .fromTo(nextActionLinks, 0.3, {y: -25, opacity: 0}, {y: 0, opacity: 1});

            changeSlide.eventCallback('onComplete', function () {
                this.slideComplete = true;
                this.startTimer();
            }.bind(this));


            $('.slides-nav a.active').removeClass('active');
            $('.slides-nav a').eq(this.slideIndex).addClass('active');
        }
    },
    slidesNav: function(newSlideNum){
        if(this.slideComplete) {
            var curIndex = this.slideIndex;
            var newIndex = newSlideNum;
            this.slideIndex = newSlideNum;

            if (curIndex !== newIndex) {
                this.slideComplete = false;
                this.stopTimer();

                var changeSlide = '';
                var currentSlide = this.slideSelector.eq(curIndex);
                var currentSlideImg = currentSlide.find('.item-img');
                var currentSlideImgSrc = currentSlide.find('.item-img-src');
                var currentTitle = currentSlide.find('.title .mask .text');
                var currentLink = currentSlide.find('.link');
                var currentActionLinks = currentSlide.find('.action-links');
                var nextSlide = this.slideSelector.eq(newIndex);
                var nextSlideImg = nextSlide.find('.item-img');
                var nextSlideImgSrc = nextSlide.find('.item-img-src');
                var nextTitle = nextSlide.find('.title .mask .text');
                var nextLink = nextSlide.find('.link');
                var nextActionLinks = nextSlide.find('.action-links');

                if (newIndex > curIndex) {

                    this.slideSelector.eq(newIndex).css({top: this.slideHeight});
                    changeSlide = new TimelineLite();
                    changeSlide.fromTo(currentSlideImg, 0.6, {scale: 1}, {scale: 0.5})

                        .fromTo(currentSlideImgSrc, 0.6, {scale: 1}, {scale: 1.5}, '-=0.6')
                        .fromTo(currentActionLinks, 0.3, {opacity: 1}, {opacity: 0}, '-=0.6')
                        .fromTo(currentLink, 0.2, {opacity: 1}, {opacity: 0})
                        .fromTo(currentTitle, 0.6, {y: '0px'}, {y: '-' + this.titleFontHeight + 'px'}, '-=0.2')
                        .to(currentSlide, 0.7, {top: -this.slideHeight, ease: Power1.easeInOut}, '-=0.6')
                        .to(nextSlide, 0.7, {top: '0px', ease: Power1.easeInOut}, '-=0.7')
                        .fromTo(nextTitle, 0.6, {y: this.titleFontHeight + 'px'}, {y: '0px'}, '-=0.3')
                        .fromTo(nextLink, 0.4, {opacity: 0}, {opacity: 1}, '-=0.4')
                        .fromTo(nextSlideImgSrc, 0.6, {scale: 1.5}, {scale: 1}, '-=0.1')
                        .fromTo(nextSlideImg, 0.6, {scale: 0.5}, {scale: 1}, '-=0.6')
                        .fromTo(nextActionLinks, 0.3, {y: -25, opacity: 0}, {y: 0, opacity: 1});

                } else {

                    this.slideSelector.eq(newIndex).css({top: -this.slideHeight});
                    changeSlide = new TimelineLite();
                    changeSlide.fromTo(currentSlideImg, 0.6, {scale: 1}, {scale: 0.5})
                        .fromTo(currentSlideImgSrc, 0.6, {scale: 1}, {scale: 1.5}, '-=0.6')
                        .fromTo(currentActionLinks, 0.3, {opacity: 1}, {opacity: 0}, '-=0.6')
                        .fromTo(currentLink, 0.2, {opacity: 1}, {opacity: 0})
                        .fromTo(currentTitle, 0.6, {y: '0px'}, {y: this.titleFontHeight + 'px'}, '-=0.2')
                        .to(currentSlide, 0.7, {top: this.slideHeight, ease: Power1.easeInOut}, '-=0.6')
                        .to(nextSlide, 0.7, {top: '0px', ease: Power1.easeInOut}, '-=0.7')
                        .fromTo(nextTitle, 0.6, {y: '-' + this.titleFontHeight + 'px'}, {y: '0px'}, '-=0.3')
                        .fromTo(nextLink, 0.4, {opacity: 0}, {opacity: 1}, '-=0.4')
                        .fromTo(nextSlideImgSrc, 0.6, {scale: 1.5}, {scale: 1}, '-=0.1')
                        .fromTo(nextSlideImg, 0.6, {scale: 0.5}, {scale: 1}, '-=0.6')
                        .fromTo(nextActionLinks, 0.3, {y: -25, opacity: 0}, {y: 0, opacity: 1});

                }

                changeSlide.eventCallback('onComplete', function(){
                    this.slideComplete = true;
                    this.startTimer();
                }.bind(this));

                $('.slides-nav a.active').removeClass('active');
                $('.slides-nav a').eq(this.slideIndex).addClass('active');
            }
        }
    },
    startTimer: function(){
        if(this.TIMER > 0){ return; }
        this.TIMER = setInterval(function(){
            this.nextSlide();
        }.bind(this), this.slideInterval);
    },
    stopTimer: function(){
        clearInterval(this.TIMER);
        this.TIMER = 0;
    }
};

//COUNTERS
var counters = {
    count_1: {val: 0, newVal: $('#counters-sect--num_1').data('count')},
    count_2: {val: 0, newVal: $('#counters-sect--num_2').data('count')},
    count_3: {val: 0, newVal: $('#counters-sect--num_3').data('count')},
    count_4: {val: 0, newVal: $('#counters-sect--num_4').data('count')},
    firstInit: false,
    init: function(){
        if(!this.firstInit) {
            TweenLite.to(this.count_1, 5, {
                val: this.count_1.newVal, roundProps: 'val', onUpdate: function () {
                    $('#counters-sect--num_1').text(this.count_1.val);
                }.bind(this)
            });
            TweenLite.to(this.count_2, 5, {
                val: this.count_2.newVal, roundProps: 'val', onUpdate: function () {
                    $('#counters-sect--num_2').text(this.count_2.val);
                }.bind(this)
            });
            TweenLite.to(this.count_3, 5, {
                val: this.count_3.newVal, roundProps: 'val', onUpdate: function () {
                    $('#counters-sect--num_3').text(this.count_3.val);
                }.bind(this)
            });
            TweenLite.to(this.count_4, 5, {
                val: this.count_4.newVal, roundProps: 'val', onUpdate: function () {
                    $('#counters-sect--num_4').text(this.count_4.val);
                }.bind(this)
            });
        }
    }
};


$(document).ready(function(){
    homeSlider.init();
    counters.init();
});
// $( window ).resize(function() {
//     location.reload();
// });
$(document).on('click', '#home-slider .navigation .next', function(){
    homeSlider.nextSlide();
});
$(document).on('click', '#home-slider .navigation .prev', function(){
    homeSlider.prevSlide();
});
$(document).on('click', '#home-slider .slides-nav a', function(){
    homeSlider.slidesNav($(this).data('slide'));
});


//update counters
$(document).ready(function(){
    setTimeout(function run() {
        update_register_users_count();
        setTimeout(run, 240000);
    }, 240000);

    setTimeout(function run() {
        update_users_online_count();
        setTimeout(run, 10000);
    }, 10000);
});


function update_register_users_count(){

    $.ajax({
        type: 'POST',
        url: ajaxurl,
        async: true,
        dataType: 'json',
        data: {
            'action': 'get_register_user_count'
        }
    }).done(function(response){
        counters.count_3.val = counters.count_3.newVal;
        counters.count_3.newVal = response.register_users;
        counters.init();
    });
}

function update_users_online_count(){

    $.ajax({
        type: 'POST',
        url: ajaxurl,
        async: true,
        dataType: 'json',
        data: {
            'action': 'get_users_online_count'
        }
    }).done(function(response){
        console.log(response);
        counters.count_4.val = counters.count_4.newVal;
        counters.count_4.newVal = response.online_users;
        counters.init();
    });
}





