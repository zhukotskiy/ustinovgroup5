$ = jQuery;
$(document).ready(function () {
    // var a = $('.um-account-main').attr('data-current_tab');
    // $('.um-account-tab[data-tab=' + a + ']').show();
    $(document).on('click', '#navigation a', function (e) {
        e.preventDefault();
        $('#navigation').find('a.active').removeClass('active');
        $(this).addClass('active');
        var curHref = $(this).attr('href');
        var curTab = $(this).attr('data-tab');
            $('input[id=_um_account_tab]:hidden').val(curTab);
            window.history.pushState('', '', curHref);
            $('.um-account-tab').hide();
            $('.um-account-tab[data-tab=' + curTab + ']').fadeIn();
            $('.um-account-nav a').removeClass('current');
            $('.um-account-nav a[data-tab=' + curTab + ']').addClass('current');
        return false;
    })
});
    // $(document).on('click', '.um-account-nav a', function (a) {
    //     a.preventDefault();
    //     var t = $(this).attr('data-tab'),
    //         u = $(this).parents('div'),
    //         e = $(this);
    //     return $('input[id=_um_account_tab]:hidden').val(t),
    //         $('.um-account-tab').hide(),
    //         e.hasClass('current') ? (u.next('.um-account-tab').slideUp(), e.removeClass('current'))  : (u.next('.um-account-tab').slideDown(), e.parents('div').find('a').removeClass('current'), e.addClass('current')),
    //         $('.um-account-side li a').removeClass('current'),
    //         $('.um-account-side li a[data-tab=' + t + ']').addClass('current'),
    //         !1
    // });