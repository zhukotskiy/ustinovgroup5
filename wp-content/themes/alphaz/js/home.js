$ = jQuery;
var sliders;
$(document).ready(function() {

    $('.fw-projects').slick({
        arrows: false,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnFocus: false
    });

    sliders = {
        endedProjects: {
            el: $('#ended-projects'),
            show: function () {
                $('#ended-projects').slick({
                        arrows: false,
                        slidesToShow: 4,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        pauseOnFocus: false,
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 3
                                }
                            },
                            {
                                breakpoint: 780,
                                settings: {
                                    slidesToShow: 2
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1
                                }
                            }
                        ]
                    });
            }
        },
        partners: {
            el: $('#partners'),
            show: function () {
                $('#partners').slick({
                        arrows: false,
                        slidesToShow: 4,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        centerMode: true,
                        variableWidth: true,
                        pauseOnFocus: false
                    });
            }
        },
        init: function() {
            var self = this;

            if (self.endedProjects.el.length > 0 && self.endedProjects.el.find('.slide-item').length > 4) {
                self.endedProjects.show();
            }

            if (self.partners.el.length > 0) {
                self.partners.show();
            }
        }
    };

    sliders.init();
});
