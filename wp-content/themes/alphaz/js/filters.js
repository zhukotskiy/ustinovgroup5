var home_filters = {
    el: $('[data-home_filters]'),
    init: function(){
        var self = this;

        self.el.each(function(){
           var cur_section = $(this);

           self.init_filters(cur_section);
        });
    },
    init_filters: function(section){
        var self = this;

        section.find('select')
            .each(function(){
                var cur_filter = $(this);
                cur_filter.change(function(){
                    self.apply_filters(section);
                });
            });
    },
    apply_filters: function(section){
        var self = this;
        var cur_filters_values = [];

        section.find('select')
            .each(function(){
                var filter_type = $(this).data('filter_type');
                var filter_value = $(this).val();
                cur_filters_values[filter_type] = filter_value;
                //cur_filters_values.push({'type': filter_type, 'value': filter_value});
            });

        var items_count = section.data('items_count');

        self.get_filtered_items(cur_filters_values, section, items_count);
    },
    get_filtered_items: function(filters, section, items_count){
        //console.log(filters['projects_cat']);
        //console.log(JSON.stringify(filters));

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            async: true,
            data: {
                'action': 'get_home_filtered_items',
                'projects_cat': filters['projects_cat'],
                'region': filters['region'],
                'projects_price': filters['projects_price'],
                'items_count': items_count,
                'project_type': section.data('project_type')
            }
        }).done(function(response){

            section.find('.items-listing')
                .empty()
                .append(response);

        });
    }
};

$(document).ready(function(){
    home_filters.init();
});