<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>USTINOV</title>
    <meta name="description" content="USTINN - самая крупная онлайн бизнес платформа объявлений Украины. Огромная база предложений по темам: готовый бизнес, стартап, кредитование, франшиза, тендеры, краудфандинг. Вы можете найти инвестора в Украине для своего бизнеса или вложить деньги в интересные проекты.">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/loader.css'?>">
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117937815-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-117937815-1');
    </script>

</head>
<body>
  <div id="header">
      <div class="container-fluid">
          <div class="row">
              <div class="col-lg-4 col">
                  <a href="<?php echo home_url(); ?>" class="logo">
                      <span>Ustinov</span>
                      <span class="small d-none d-xl-inline-block">Время правильных решений</span>
                  </a>
              </div>
              <div class="header-navigation col d-none d-md-flex"><?php do_action('get_header_menu'); ?></div>
              <div class="user-nav col-lg-4 col">
                  <a href="<?php echo home_url(). '/profile/user-projects?action=add_new_project'; ?>" class="header-icon full-h d-none d-lg-flex">
                      <span class="add-project-btn">Добавить проект</span>
                  </a>
                  <a href="http://ustinn.com/profile/user-projects?action=add_new_project" class="header-icon d-flex d-lg-none">
                      <span class="lnr lnr-cross" style="transform: rotate(45deg);"></span>
                  </a>
                  <a href="javascript:void(0)" class="header-icon" data-modal_link="search">
                      <i class="lnr lnr-magnifier"></i>
                  </a>
                  <?php if(is_user_logged_in()): ?>
                      <a href="<?php echo Users_Actions::get_mailbox_link(); ?>" class="header-icon d-none d-md-flex">
                          <i class="fi_2 flaticon-mail"></i>
                      </a>
                      <a href="<?php echo Users_Actions::get_projects_link(); ?>" class="header-icon d-none d-md-flex">
                          <i class="lnr lnr-user"></i>
                      </a>
                  <?php else: ?>
                      <a href="<?php echo site_url(). '/auth'?>" class="header-icon login-in d-none d-md-flex">
                          <span>Войти</span>
                      </a>
                  <?php endif; ?>
                  <div class="header-icon d-flex d-md-none">
                      <div id="mobile-menu">
                          <span class="line"></span>
                          <span class="line"></span>
                          <span class="line"></span>
                        <div class="menu-section">
                            <?php do_action('get_header_menu'); ?>
                            <ul>
	                            <?php if(is_user_logged_in()): ?>
                                <li>
                                    <a href="<?php echo Users_Actions::get_projects_link(); ?>">Профиль</a>
                                    <ul>
                                        <li><a href="<?php echo Users_Actions::get_mailbox_link(); ?>">Сообщения</a></li>
                                    </ul>
                                </li>
                                <?php else: ?>
                                    <li>
                                        <a href="<?php echo site_url(). '/auth'?>"><span>Войти</span></a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

