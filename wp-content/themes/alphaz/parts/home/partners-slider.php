<section class="light">
	<div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">Наши партнеры</h1>
            </div>
        </div>
		<div id="partners" class="row">
			<?php foreach($posts as $post): ?>
				<?php
					$thumbnail_url = null;
					$thumbnail_id = get_post_meta($post->ID, 'company_logo', true);
					if(!empty($thumbnail_id)){
						$thumbnail_url = wp_get_attachment_image_url($thumbnail_id, 'medium');
						if(empty($thumbnail_url)) $thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
					}else{
						$thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
					}
				?>
				<div class="item">
					<img src="<?php echo $thumbnail_url; ?>" alt="">
				</div>
			<?php endforeach; ?>

		</div>
	</div>
</section>