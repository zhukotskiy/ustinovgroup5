<section class="light">
    <div class="container">
        <div class="row no-margin">
            <div class="col-12">
                <h1 class="text-center">Успешно проинвестированные проекты</h1>
            </div>
        </div>
        <div id="ended-projects" class="row d-flex no-margin">
		<?php
			foreach($posts as $index => $post){
				include get_template_directory(). '/parts/loop/project-loop-min.php';
			}
		?>
		</div>
    </div>
</section>