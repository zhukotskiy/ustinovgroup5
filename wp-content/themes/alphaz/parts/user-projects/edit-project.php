<form name="Project" id="edit-project" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" onSubmit="checkForm(); return(false);">
	<input type="hidden" name="action" value="user_save_post">
	<input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
	<div class="row">
		<div class="col-md-6">
            <div class="input-field">
                <label for="post_title">Название проекта (максимум 52 символов)</label>
                <input type="text" name="post_title" id="post_title" placeholder="Новый проект" value="<?php echo $post->post_title; ?>" maxlength="52" required>
            </div>
        </div>
        <?php
            $args = array(
                'taxonomy' => 'projects_type',
                'hide_empty' => false,
            );
            $project_types = get_terms($args);
            $cur_project_type = wp_get_post_terms($post->ID, 'projects_type', array('fields' => 'ids'))[0];
        ?>
        <?php if(!empty($project_types)): ?>
            <div class="col-md-6">
                <div class="input-field">
                    <label for="project_type">Тип проекта</label>
                    <select name="project_type" id="project_type">
                        <?php foreach($project_types as $type): ?>
                            <?php if(get_term_meta($type->term_id, 'show_in_frontend', true)): ?>
                                <option value="<?php echo $type->term_id; ?>" <?php echo ($cur_project_type == $type->term_id) ? 'selected' : ''; ?>><?php echo $type->name; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        <?php endif; ?>

		<?php
            $args = array(
                'taxonomy' => 'projects_cat',
                'hide_empty' => false,
            );
            $project_cat = get_terms($args);
		    $cur_project_cat = wp_get_post_terms($post->ID, 'projects_cat', array('fields' => 'ids'))[0];
		?>
		<?php if(!empty($project_cat)): ?>
            <div class="col-md-6">
                <div class="input-field">
                    <label for="project_cat">Категория</label>
                    <select name="projects_cat" id="project_cat">
                        <option>Без категории</option>
						<?php foreach($project_cat as $cat): ?>

                            <option value="<?php echo $cat->term_id; ?>" <?php echo ($cur_project_cat == $cat->term_id) ? 'selected' : ''; ?>><?php echo $cat->name; ?></option>
						<?php endforeach; ?>
                    </select>
                </div>
            </div>
		<?php endif; ?>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="area-field">
				<label for="short_description">Краткое описание</label>
				<textarea type="text" name="short_description" id="short_description" required><?php echo get_post_meta($post->ID, 'short_description', true); ?></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 pb-30">
			<div class="area-field">
				<label>Полное описание</label>
				<?php wp_editor( $post->post_content, 'postedit', array( 'media_buttons' => false )); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<?php
            $args = array(
                'taxonomy' => 'region',
                'hide_empty' => false,
            );
            $regions = get_terms($args);
            $cur_project_region = wp_get_post_terms($post->ID, 'region', array('fields' => 'ids'))[0];
		?>
		<?php if(!empty($regions)): ?>
            <div class="col-md-6">
                <div class="input-field">
                    <label for="project_region">Регион</label>
                    <select name="projects_region" id="project_region">
                        <option>Выберите регион</option>
						<?php foreach($regions as $region): ?>
                            <option value="<?php echo $region->term_id; ?>" <?php echo ($cur_project_region == $region->term_id) ? 'selected' : ''; ?>><?php echo $region->name; ?></option>
						<?php endforeach; ?>
                    </select>
                </div>
            </div>
		<?php endif; ?>
		<div class="col-md-6">
			<div class="input-field">
				<label for="phone">Контактный телефон</label>
				<input type="text" name="phone" id="phone" placeholder="+38 (000) 000-0000" value="<?php echo get_post_meta($post->ID, 'phone', true); ?>" required>
			</div>
		</div>
		<div class="col-md-6">
			<div class="input-field">
				<label for="project_link">Ссылка на сайт</label>
				<input type="text" name="project_link" id="project_link" placeholder="ustinn.com" value="<?php echo get_post_meta($post->ID, 'project_link', true); ?>">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="input-field">
				<label for="invest_sum">Цена</label>
				<input type="text" name="invest_sum" id="invest_sum" placeholder="1 000 000$" value="<?php echo get_post_meta($post->ID, 'invest_sum', true); ?>" required>
			</div>
		</div>
        <div class="col-md-6">
            <div class="input-field">
                <label for="currency">Валюта</label>
	            <?php $currency = get_post_meta($post->ID, 'currency', true); ?>
                <select id="currency" name="currency">
                    <option value="usd" <?php echo (empty($currency) || $currency == 'usd') ? 'selected' : ''; ?>>Доллар</option>
                    <option value="uah" <?php echo ($currency == 'uah') ? 'selected' : ''; ?>>Гривна</option>
                    <option value="eur" <?php echo ($currency == 'eur') ? 'selected' : ''; ?>>Евро</option>
                </select>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col-12 pb-30">
            <div class="divider"></div>
        </div>
    </div>
    <div id="thumbnail-setup" class="row files-area">
        <label class="col-12 field-name">Главное фото</label>
        <?php $thumbnail = get_post_meta($post->ID, '_thumbnail_id', true); ?>
        <input type="hidden" id="thumbnail" name="thumbnail" value="<?php echo $thumbnail; ?>">
        <?php
            $thumbnail_id = get_post_thumbnail_id($post->ID);
            $thumbnail_url = wp_get_attachment_image_url($thumbnail_id, 'large');
        ?>
        <?php if(!empty($thumbnail_url)): ?>
            <div class="col-md-3 file-item pb-4" data-img_id="<?php echo $thumbnail_id; ?>">
                <div class="file-item-prev" style="background-image: url(<?php echo $thumbnail_url; ?>)">
                   <i class="material-icons shortcut-icon delete remove-item">delete</i>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-3 add-file pb-4">
            <div class="file-input">
                <input type="file" name="project_thumbnail" id="thumbnail_new_item">
                <label for="thumbnail_new_item"><i class="material-icons">add</i></label>
            </div>
        </div>
    </div>
    <div id="error-message" class="row files">
        <div class="col-12">
            <div class="message"><i class="material-icons">error_outline</i>
                <p>Добавьте фото</p>
            </div>
        </div>
    </div>
    <div id="gallery-setup" class="row files-area">
        <label class="col-12 field-name">Галлерея</label>
	    <?php
            $gallery_arr = get_post_meta($post->ID, '_gallery', true);
            $gallery = '';
            if(!empty($gallery_arr)) $gallery = json_encode($gallery_arr);
	    ?>
        <input type="hidden" id="gallery" name="gallery" value="<?php echo $gallery; ?>">
	    <?php $gallery = get_gallery($post->ID); ?>
	    <?php if (!empty($gallery)): ?>
		    <?php foreach($gallery as $item): ?>
                <div class="col-md-3 file-item pb-4" data-img_id="<?php echo $item->ID; ?>">
                    <div class="file-item-prev" style="background-image: url(<?php echo $item->large_url; ?>)">
                        <i class="material-icons shortcut-icon delete remove-item">delete</i>
                    </div>
                </div>
		    <?php endforeach; ?>
	    <?php endif; ?>

        <div class="col-md-3 add-file pb-4">
            <div class="file-input">
                <input type="file" name="project_thumbnail" id="gallery_new_item">
                <label for="gallery_new_item"><i class="material-icons">add</i></label>
            </div>
        </div>
    </div>
	<div class="row mt-3">
		<div class="col-12 d-flex justify-content-end">
			<input type="submit" class="filled-btn" value="Обновить">
		</div>
	</div>
</form>

<script>
    function checkForm(){
        var obj_form = document.forms.Project;
        var thumbnail = $('#thumbnail');
        if(thumbnail.val() === ''){
            $('#error-message').fadeIn();
            return;
        }
        obj_form.submit();
    }
</script>