<a href="?action=edit&project_id=<?php echo $post->ID; ?>" class="project-item pt-3 pb-3 d-flex flex-wrap">
	<div class="col-12 col-sm-4 col-lg-2 p-0 text-center">
		<div class="logo-prev" style="background-image: url(<?php echo get_thumbnail($post->ID); ?>)"></div>
	</div>
	<div class="col-12 col-sm-8 col-lg-10">
		<div class="col-12 col-sm-12 p-0">
			<h4 class="mt-0 p-0"><?php echo (!empty($post->post_title)) ? $post->post_title : 'Название проекта'; ?></h4>
		</div>
		<div class="row p-0">
			<div class="col-sm-12 col-md-6 col-lg-4">
                <h6 class="m-0 "><?php echo get_styled_post_status($post->post_status); ?></h6>
            </div>
			<div class="col-sm-12 col-md-6 col-lg-2 text-center d-flex align-items-center mb-3 mb-md-4 mb-lg-0">
                <h6 class="m-0"><?php echo (!empty(get_post_meta($post->ID, 'invest_sum', true))) ? get_post_meta($post->ID, 'invest_sum', true) : 0; ?></h6>
            </div>
			<div class="col-4 col-sm-4 mt-2 m-sm-0 col-lg-2 text-center d-flex align-items-center">
                <span class="stroke-icon lnr lnr-eye mr-16"></span>
                <h6 class="m-0"><?php echo (!empty(get_post_meta($post->ID, 'views', true))) ? get_post_meta($post->ID, 'views', true) : 0; ?></h6>
            </div>
			<div class="col-4 col-sm-4 mt-2 m-sm-0 col-lg-2 text-center d-flex align-items-center">
                <span class="stroke-icon lnr lnr-phone-handset mr-16"></span>
                <h6 class="m-0"><?php echo (!empty(get_post_meta($post->ID, 'get_phone', true))) ? get_post_meta($post->ID, 'get_phone', true) : 0; ?></h6>
            </div>
			<div class="col-4 col-sm-4 mt-2 m-sm-0 col-lg-2 text-center d-flex align-items-center">
                <span class="stroke-icon lnr lnr-star mr-16"></span>
                <h6 class="m-0"><?php echo (!empty(get_post_favorites_count($post->ID))) ? get_post_favorites_count($post->ID) : 0; ?></h6>
            </div>
		</div>
        <div class="row mt-4">
            <div class="col-12 d-flex justify-content-end">
                <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
                    <input type="hidden" name="action" value="remove_user_project">
                    <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                    <input type="submit" class="filled-btn--small remove" value="Удалить">
                </form>
            </div>
        </div>
	</div>
</a>