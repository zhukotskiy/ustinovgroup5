<div class="col-md-6 col-lg-4 col-xl-3 slide-item">
	<a href="<?php echo get_permalink($post->ID); ?>" class="project-card mb-2 mt-3">
		<div class="img-prev" style="background-image: url(<?php echo get_thumbnail($post->ID, 'medium'); ?>)">
			<?php echo get_favorites_button($post->ID);?>
		</div>
        <div class="info">
            <div class="main">
                <div class="title"><?php echo $post->post_title; ?></div>
                <div class="description"><?php echo get_post_meta($post->ID, 'short_description', true); ?></div>
            </div>
            <div class="additional">
                <?php $price = get_post_meta($post->ID, 'invest_sum', true); ?>
                <?php $currency_val = ''; ?>
                <?php if($price != 'Договорная' && !empty($price) && $price != 0): ?>
                    <?php $prepended_price = preg_replace('~\D+~', '', $price); ?>
                    <?php $formated_price = number_format($prepended_price, 0, ',', ' '); ?>
                    <?php $currency = get_post_meta($post->ID, 'currency', true); ?>
                    <?php
                        if($currency == 'usd' || empty($currency)){
                            $currency_val = ' $';

                        }else if($currency == 'uah'){
                            $currency_val = ' грн';

                        }else if($currency == 'eur'){
                            $currency_val = ' €';

                        }
                    ?>
                    <div class="price"><?php echo $formated_price. $currency_val; ?></div>
                <?php else: ?>
                    <div class="price">Договорная</div>
                <?php endif; ?>

                <?php $views = get_post_meta($post->ID, 'views', true); ?>
                <div class="views"><span class="lnr lnr-eye"></span><?php echo (!empty($views)) ? $views : 0; ?></div>
            </div>
        </div>
	</a>
</div>