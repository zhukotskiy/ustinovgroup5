<span class="info">
    <h3 class="mt-3 title"><?php echo $post->post_title; ?></h3>

    <p class="mt-1 description"><?php echo get_post_meta($post->ID, 'short_description', true); ?></p>
    <?php if(!empty($invest_type) || !empty($invest_sum)): ?>
        <div class="mt-3 pt-1 bottom d-flex">
            <?php if($invest_type == 'type_1' || $invest_type == 'type_2'): ?>
                <span class="bottom-left d-flex flex-column">
                    <?php if($invest_type == 'type_1'): ?>
                        <span class="text-blue"><?php echo get_post_meta($post->ID, 'proposed_share', true). ' %'; ?></span>
                        <span>Доля</span>
                    <?php elseif($invest_type == 'type_2'): ?>
                        <span class="text-blue"><?php echo get_post_meta($post->ID, 'recommend_bid', true). ' %'; ?></span>
                        <span>Ставка</span>
                    <?php endif; ?>
                </span>
                <?php if(!empty($invest_sum)): ?>
                    <div class="bottom-right d-flex flex-column ml-2 pl-2">
                        <span class="text-blue"><?php echo $invest_sum. ' $'; ?></span>
                        <span>Необходимые инвестиции</span>
                    </div>
                <?php endif;?>
            <?php else: ?>
                <?php if(!empty($invest_sum)): ?>
                    <div class="bottom-left d-flex flex-column">
                        <span class="text-blue"><?php echo $invest_sum. ' $'; ?></span>
                        <span>Необходимые инвестиции</span>
                    </div>
                <?php endif;?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</span>