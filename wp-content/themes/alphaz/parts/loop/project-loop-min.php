<div class="col-lg-3 slide-item">
	<a href="<?php echo get_post_meta($post->ID, 'project_link', true); ?>" class="project-card mb-2 mt-3" target="_blank">
		<div class="img-prev" style="background-image: url(<?php echo get_thumbnail($post->ID, 'large'); ?>)"></div>
	</a>
</div>