<?php
	$invest_sum = get_post_meta($post->ID, 'invest_sum', true);
	$invest_already = get_post_meta($post->ID, 'invest_already', true);
	$investors_count = get_post_meta($post->ID, 'investors_count', true);
	$views = get_post_meta($post->ID, 'views', true);

	$progress = 0;
	if(!empty($invest_already) && !empty($invest_sum)){
		$progress = intval((100 * intval(preg_replace("/[^,.0-9]/", '', $invest_already))) / intval(preg_replace("/[^,.0-9]/", '', $invest_sum)) );
	}
?>
<div class="col-12">
	<div class="fw-card">
		<div class="row no-margin">
			<div class="col-lg-6 project-prev" style="background-image: url(<?php echo get_thumbnail($post->ID, 'large'); ?>)"></div>
			<div class="col-lg-6">
				<div class="title"><?php echo $post->post_title; ?></div>
				<div class="invest-already"><?php echo $invest_already; ?></div>
				<div class="progress-bar">
					<div class="progress" style="width: <?php echo $progress. '%'; ?>"></div>
				</div>
				<div class="invest-already-info">
					<span>собрано из </span>
					<span><?php echo $invest_sum; ?></span>
				</div>
				<div class="description"><?php echo get_post_meta($post->ID, 'short_description', true); ?></div>
				<div class="additional-info">
                    <div class="additional-info-item">
                        <span class="lnr lnr-users"></span>
                        <span class="text"><?php echo (!empty($investors_count)) ? $investors_count : 0; ?></span>
                    </div>
                    <div class="additional-info-item">
                        <span class="lnr lnr-eye"></span>
                        <span class="text"><?php echo (!empty($views)) ? $views : '0'; ?></span>
                    </div>
                </div>
                <div class="d-flex justify-content-end action">
					<a href="<?php echo get_permalink($post->ID); ?>" class="filled-btn--small shadow">Подробнее</a>
				</div>
			</div>
		</div>
	</div>
</div>