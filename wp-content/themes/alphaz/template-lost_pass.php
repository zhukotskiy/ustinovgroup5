<?php
/**
 *template name: Lost Pass
 **/
?>
<?php get_header('empty'); ?>
	<section id="auth">
		<div class="auth-body">
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
				<a href="<?php echo get_site_url(); ?>" class="go-home"><span class="lnr lnr-cross "></span></a>
				<div class="logo">USTINN</div>
				<input type="hidden" name="action" value="lost_pass">
				<input type="email" name="user_login" class="auth-input" placeholder="Email" required>
				<?php if($_GET['register_error'] == 'existing_user_email'): ?>
					<div class="error_message">Пользователь с таким email уже существует</div>
				<?php endif; ?>
				<input type="submit" class="auth-submit" value="Восстановить">
			</form>
		</div>
	</section>
<?php get_footer('empty'); ?>