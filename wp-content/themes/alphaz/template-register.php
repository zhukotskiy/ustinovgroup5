<?php
/**
 *template name: Register
 **/
?>
<?php get_header('empty'); ?>
	<section id="auth">
		<div class="auth-body">
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
				<a href="<?php echo get_site_url(); ?>" class="go-home"><span class="lnr lnr-cross "></span></a>
				<div class="logo">USTINN</div>
				<input type="hidden" name="action" value="user_register">
				<input type="text" name="user_first_name" class="auth-input" placeholder="Имя" required>
				<input type="email" name="user_email" class="auth-input" placeholder="Email" required>
				<?php if($_GET['register_error'] == 'existing_user_email'): ?>
					<div class="error_message">Пользователь с таким email уже существует</div>
                <?php elseif($_GET['register_error'] == 'email_is_incorrect'): ?>
                    <div class="error_message">Введен не корректный email</div>
				<?php endif; ?>
				<input type="text" name="user_pass"  class="auth-input" placeholder="Password" required>
                <button class="auth-submit">Продолжить</button>
				<div class="create-acc text-center">
					<span>Аккаунт уже существует?</span>
					<a href="<?php echo get_site_url(). '/auth'; ?>">Авторизироваться</a>
				</div>
<!--                <div class="divider mt-5"></div>-->
<!--                <div class="create-acc text-center mt-4">-->
<!--                    <span>Забыли пароль?</span>-->
<!--                    <a href="--><?php //echo get_site_url(). '/lost_pass'; ?><!--">Восстановить</a>-->
<!--                </div>-->
			</form>
		</div>
	</section>
<?php get_footer('empty'); ?>