<?php
/**
 *template name: Full Width
 **/
?>
<!-- Header -->
<?php get_header(); ?>
<div class="wrapper">
	<?php if(have_posts()): ?>
		<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
<!-- Footer -->
<?php get_footer(); ?>
