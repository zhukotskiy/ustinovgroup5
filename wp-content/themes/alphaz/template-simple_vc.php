<?php
	/*
	Template Name: Sipmple VC
	*/
?>
<?php
	if(!is_admin()){
		//styles
		wp_enqueue_style('vc_bootstrap-css');
		wp_enqueue_style('vc_animate-css');
		wp_enqueue_style('vc_slick-css');
		wp_enqueue_style('vc_slick-css-theme');
		wp_enqueue_style('vc_main-css');
		wp_enqueue_style('vc_style');
		//scripts
		wp_enqueue_script('jquery');
		wp_enqueue_script('vc_slick-js');
		wp_enqueue_script('vc_wow-js');
		wp_enqueue_script('vc_scroll-magic-js');
		wp_enqueue_script('vc_map-js');
		wp_enqueue_script('vc_main-js');
	}
?>
<?php get_header(); ?>
	<div class="container-fluid">
		<?php if(have_posts()): ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>