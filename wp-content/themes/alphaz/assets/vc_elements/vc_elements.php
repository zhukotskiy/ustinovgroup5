<?php
//STYLES & SCRIPTS
//styles
wp_register_style('vc_style', get_stylesheet_directory_uri(). '/assets/vc_elements/css/style.css');
wp_register_style('vc_bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css');
wp_register_style('vc_main-css', get_stylesheet_directory_uri(). '/assets/vc_elements/css/main.css');
wp_register_style('vc_animate-css', get_stylesheet_directory_uri(). '/assets/vc_elements/css/animate.css');
wp_register_style('vc_slick-css', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css');
wp_register_style('vc_slick-css-theme', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css');

//scripts
wp_register_script('vc_main-js', get_stylesheet_directory_uri() . '/assets/vc_elements/js/main.js', 'jquery, wow-js, scroll-magic-js', '1.0.0', TRUE);
wp_register_script('vc_wow-js', get_stylesheet_directory_uri() . '/assets/vc_elements/js/wow.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('vc_slick-js', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('vc_map-js', get_stylesheet_directory_uri() . '/assets/vc_elements/js/map.js', 'jquery', '1.0.0', TRUE);
wp_register_script('vc_scroll-magic-js', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.js', 'jquery', '1.0.0', TRUE);

add_action('wp_ajax_nopriv_more_holding_projects', 'get_more_holding_projects');
add_action('wp_ajax_more_holding_projects', 'get_more_holding_projects');
function get_more_holding_projects(){
	$args = array(
		'post_type'         => 'holding_companies',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'posts_per_page'    => 8,
		'offset'            => $_POST['count']
	);
	$query = new WP_Query;
	$posts = $query->query($args);

	if(!empty($posts)){
		foreach($posts as $post){
			$animationDelay = 0;
			include get_template_directory(). '/assets/vc_elements/templates/projects__loop.php';
		}
	}

	if(count($posts) < 8 || empty($posts)){
		echo '<script> $("#more-projects").fadeOut()</script>';
	}else{
		echo '<script> $("#projects-count").val('. (intval($_POST['count']) + 8). ')</script>';
	}

	die();
}

//SECTION 1
vc_map(
    array(
        'name'          => __('Блок с видео', 'wpcentral'),
        'description'   => __('Полупрозрачный блок с заголовком', 'wpcentral'),
        'base'          => __('header_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Вторичный текст', 'wpcentral'),
                                'param_name'    => 'secondary_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true,
                            )
                        )
    )
);

add_shortcode('header_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'main_title' => 'Основной заголовок',
    'secondary_title' => 'Вторичный заголовок'
  ), $atts ) );

  if(!empty($main_title)){
      $title = explode(' ', $main_title);
      $main_title = '';
      foreach($title as $index => $titlePart){
        if($index == count($title) - 1){
            $main_title .= $titlePart;
        }else{
            $main_title .= $titlePart. '<br> ';
        }
      }
    }

  include( locate_template( 'assets/vc_elements/templates/video_section.php')); 
});

//SECTION 2
vc_map(
    array(
        'name'          => __('Блок с картинками', 'wpcentral'),
        'description'   => __('Анимированій блок с картинками', 'wpcentral'),
        'base'          => __('img_prev_section', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Вторичный текст', 'wpcentral'),
                                'param_name'    => 'secondary_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 1', 'wpcentral'),
                                'param_name'    => 'img_1',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 2', 'wpcentral'),
                                'param_name'    => 'img_2',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 3', 'wpcentral'),
                                'param_name'    => 'img_3',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 4', 'wpcentral'),
                                'param_name'    => 'img_4',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 5', 'wpcentral'),
                                'param_name'    => 'img_5',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 6', 'wpcentral'),
                                'param_name'    => 'img_6',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 7', 'wpcentral'),
                                'param_name'    => 'img_7',
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 8', 'wpcentral'),
                                'param_name'    => 'img_8',
                            )
                        )
    )
);

add_shortcode('img_prev_section', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'main_title' => 'Основной текст',
    'secondary_title' => 'Вторичный текст',
    'img_1' => '',
    'img_2' => '',
    'img_3' => '',
    'img_4' => '',
    'img_5' => '',
    'img_6' => '',
    'img_7' => '',
    'img_8' => ''
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/img_prev_section.php')); 
});

//SECTION 3
vc_map(
    array(
        'name'          => __('Слайдер категорий', 'wpcentral'),
        'description'   => __('Анимированый слайдер', 'wpcentral'),
        'base'          => __('category_slider', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            )
                        )
    )
);

add_shortcode('category_slider', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'main_title' => 'Основной текст',
    
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/category_slider.php')); 
});

//SECTION 4
vc_map(
    array(
        'name'          => __('Блок с иконками', 'wpcentral'),
        'description'   => __('Серый блок с иконками', 'wpcentral'),
        'base'          => __('busines_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Название раздела', 'wpcentral'),
                                'param_name'    => 'section_name',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Заголовок', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на фоне', 'wpcentral'),
                                'param_name'    => 'bg_text',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст блок 1', 'wpcentral'),
                                'param_name'    => 'text_1',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст блок 2', 'wpcentral'),
                                'param_name'    => 'text_2',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст блок 3', 'wpcentral'),
                                'param_name'    => 'text_3',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            )
                        )
    )
);

add_shortcode('busines_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'section_name'  => 'Название раздела',
    'main_title'    => 'Заголовок',
    'bg_text'       => 'Текст на фоне',
    'text_1'        => 'Текст блок 1',
    'text_2'        => 'Текст блок 2',
    'text_3'        => 'Текст блок 3',
    
  ), $atts ) );

    if(!empty(main_title)){
      $title = explode(' ', $main_title);
      $main_title = '';
      $secondary_title = '';
      foreach($title as $index => $titlePart){
        if($index > 3){
            $secondary_title .= $titlePart. ' ';
        }else{
            $main_title .= $titlePart. ' ';
        }
      }
    }

  include( locate_template( 'assets/vc_elements/templates/busines_block.php')); 
});

//SECTION 5
vc_map(
    array(
        'name'          => __('Серый блок', 'wpcentral'),
        'description'   => __('Серый анимированый блок', 'wpcentral'),
        'base'          => __('grey_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Название раздела', 'wpcentral'),
                                'param_name'    => 'section_name',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Заголовок', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на фоне', 'wpcentral'),
                                'param_name'    => 'bg_text',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textarea',
                                'heading'       => __('Описание', 'wpcentral'),
                                'param_name'    => 'description',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            )
                        )
    )
);

add_shortcode('grey_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'section_name'  => 'Название раздела',
    'main_title'    => 'Заголовок',
    'bg_text'       => 'Текст на фоне',
    'description'   => 'Текст'
    
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/gray_block.php')); 
});

//SECTION 6
vc_map(
    array(
        'name'          => __('Блок проектов', 'wpcentral'),
        'description'   => __('Большей блок с проектами', 'wpcentral'),
        'base'          => __('projects_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Вторичный текст', 'wpcentral'),
                                'param_name'    => 'secondary_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Количество карточек', 'wpcentral'),
                                'param_name'    => 'cards_count',
                                'value'         => __('12', 'wpcentral')
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на кнопке', 'wpcentral'),
                                'param_name'    => 'btn_text',
                                'value'         => __('Все проекты', 'wpcentral')
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Ссылка', 'wpcentral'),
                                'param_name'    => 'btn_link',
                                'value'         => __('', 'wpcentral')
                            )
                        )
    )
);

add_shortcode('projects_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'main_title'        => 'Основной заголовок',
    'secondary_title'   => 'Вторичный заголовок',
    'cards_count'       => '12',
    'btn_text'          => 'Все проекты',
    'btn_link'          => '/projects'
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/projects_block.php')); 
});

//SECTION 7
vc_map(
    array(
        'name'          => __('Вертикальный с иконками', 'wpcentral'),
        'description'   => __('Вертикальный блок с иконками', 'wpcentral'),
        'base'          => __('vertical_icon_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Заголовок', 'wpcentral'),
                                'param_name'    => 'title_1',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 1',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textarea',
                                'heading'       => __('Описание', 'wpcentral'),
                                'param_name'    => 'description_1',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 1',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Заголовок', 'wpcentral'),
                                'param_name'    => 'title_2',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 2',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textarea',
                                'heading'       => __('Описание', 'wpcentral'),
                                'param_name'    => 'description_2',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 2',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Заголовок', 'wpcentral'),
                                'param_name'    => 'title_3',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 3',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textarea',
                                'heading'       => __('Описание', 'wpcentral'),
                                'param_name'    => 'description_3',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 3',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Заголовок', 'wpcentral'),
                                'param_name'    => 'title_4',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 4',
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textarea',
                                'heading'       => __('Описание', 'wpcentral'),
                                'param_name'    => 'description_4',
                                'value'         => __('Текст', 'wpcentral'),
                                'group' => 'Секция 4',
                                'admin_label' => true
                            )
                        )
    )
);

add_shortcode('vertical_icon_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'title_1'           => 'Заголовок',
    'description_1'     => 'Описание',
    'title_2'           => 'Заголовок',
    'description_2'     => 'Описание',
    'title_3'           => 'Заголовок',
    'description_3'     => 'Описание',
    'title_4'           => 'Заголовок',
    'description_4'     => 'Описание'
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/vertical_icon_block.php')); 
});


//SECTION 8
vc_map(
    array(
        'name'          => __('Блок с комментарием', 'wpcentral'),
        'description'   => __('Блок с комментарием', 'wpcentral'),
        'base'          => __('simple_comment_block_test', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Фото', 'wpcentral'),
                                'param_name'    => 'img',
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Имя', 'wpcentral'),
                                'param_name'    => 'name',
                                'value'         => __('Виктор', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Должность', 'wpcentral'),
                                'param_name'    => 'position',
                                'value'         => __('Менеджер по развитию бизнеса', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textarea',
                                'heading'       => __('Комментарий', 'wpcentral'),
                                'param_name'    => 'comment',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на кнопке', 'wpcentral'),
                                'param_name'    => 'btn_text',
                                'value'         => __('Консультация', 'wpcentral')
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Ссылка', 'wpcentral'),
                                'param_name'    => 'btn_link',
                                'value'         => __('', 'wpcentral'),
                            )
                        )
    )
);

add_shortcode('simple_comment_block_test', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'img'          => '',
    'name'         => 'Имя',
    'position'     => 'Должность',
    'comment'      => 'Комментарий',
    'btn_text'     => 'Описание',
    'btn_link'     => '/more'
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/comment_block.php')); 
});

//SECTION 9
vc_map(
    array(
        'name'          => __('Блок с картой', 'wpcentral'),
        'description'   => __('Блок с картой', 'wpcentral'),
        'base'          => __('simple_map_section', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true,
                                'group' => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Вторичный текст', 'wpcentral'),
                                'param_name'    => 'secondary_title',
                                'value'         => __('Текст', 'wpcentral'),
                                'admin_label' => true,
                                'group' => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на кнопке', 'wpcentral'),
                                'param_name'    => 'btn_text',
                                'value'         => __('Консультация', 'wpcentral'),
                                'group' => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Ссылка', 'wpcentral'),
                                'param_name'    => 'btn_link',
                                'value'         => __('', 'wpcentral'),
                                'group' => 'Основные'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Австралия', 'wpcentral'),
                                'param_name'    => 'img_1',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Аргентина', 'wpcentral'),
                                'param_name'    => 'img_2',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Бразилия', 'wpcentral'),
                                'param_name'    => 'img_3',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Великобритания', 'wpcentral'),
                                'param_name'    => 'img_4',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Германия', 'wpcentral'),
                                'param_name'    => 'img_5',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Индия', 'wpcentral'),
                                'param_name'    => 'img_6',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Индонезия', 'wpcentral'),
                                'param_name'    => 'img_7',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Италия', 'wpcentral'),
                                'param_name'    => 'img_8',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Канада', 'wpcentral'),
                                'param_name'    => 'img_9',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Китай', 'wpcentral'),
                                'param_name'    => 'img_10',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Мексика', 'wpcentral'),
                                'param_name'    => 'img_11',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Россия', 'wpcentral'),
                                'param_name'    => 'img_12',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Саудовская Аравия', 'wpcentral'),
                                'param_name'    => 'img_13',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('США', 'wpcentral'),
                                'param_name'    => 'img_14',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Турция', 'wpcentral'),
                                'param_name'    => 'img_15',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Франция', 'wpcentral'),
                                'param_name'    => 'img_16',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Южная Корея', 'wpcentral'),
                                'param_name'    => 'img_17',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('ЮАР', 'wpcentral'),
                                'param_name'    => 'img_18',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Япония', 'wpcentral'),
                                'param_name'    => 'img_19',
                                'group' => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Украина', 'wpcentral'),
                                'param_name'    => 'img_20',
                                'group' => 'Картинки'
                            )

                        )
    )
);

add_shortcode('simple_map_section', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'main_title' => 'Основной заголовок',
    'secondary_title' => 'Вторичный заголовок',
    'img_1' => '',
    'img_2' => '',
    'img_3' => '',
    'img_4' => '',
    'img_5' => '',
    'img_6' => '',
    'img_7' => '',
    'img_8' => '',
    'img_9' => '',
    'img_10' => '',
    'img_11' => '',
    'img_12' => '',
    'img_13' => '',
    'img_14' => '',
    'img_15' => '',
    'img_16' => '',
    'img_17' => '',
    'img_18' => '',
    'img_19' => '',
    'img_20' => ''
  ), $atts ) );

    if(!empty(main_title)){
      $title = explode(' ', $secondary_title);
      $secondary_title_1 = '';
      $secondary_title_2 = '';
      foreach($title as $index => $titlePart){
        if($index > 8){
            $secondary_title_2 .= $titlePart. ' ';
        }else{
            $secondary_title_1 .= $titlePart. ' ';
        }
      }
    }

  include( locate_template( 'assets/vc_elements/templates/simple_map_block.php')); 
});

//SECTION 10
vc_map(
    array(
        'name'          => __('Синий блок', 'wpcentral'),
        'base'          => __('blue_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Название секции', 'wpcentral'),
                                'param_name'    => 'section_title',
                                'value'         => __('Раздел', 'wpcentral'),
                                'admin_label' => true,
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Основной текст', 'wpcentral'),
                                'admin_label' => true,
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на кнопке', 'wpcentral'),
                                'param_name'    => 'btn_text',
                                'value'         => __('Подробней', 'wpcentral'),
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Ссылка', 'wpcentral'),
                                'param_name'    => 'btn_link',
                                'value'         => __('', 'wpcentral'),
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 1', 'wpcentral'),
                                'param_name'    => 'img_1',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 2', 'wpcentral'),
                                'param_name'    => 'img_2',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 3', 'wpcentral'),
                                'param_name'    => 'img_3',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 4', 'wpcentral'),
                                'param_name'    => 'img_4',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 5', 'wpcentral'),
                                'param_name'    => 'img_5',
                                'group'         => 'Картинки'
                            )
                        )
    )
);

add_shortcode('blue_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'section_title' => 'Раздел',
    'main_title'    => 'Основной текст',
    'img_1'         => '',
    'img_2'         => '',
    'img_3'         => '',
    'img_4'         => '',
    'img_5'         => '',
    'btn_text'     => 'Подробней',
    'btn_link'     => '/more'
  ), $atts ) );

    if(!empty($main_title)){
      $title = explode(' ', $main_title);
      $main_title_1 = '';
      $main_title_2 = '';
      foreach($title as $index => $titlePart){
        if($index > 7){
            $main_title_2 .= $titlePart. ' ';
        }else{
            $main_title_1 .= $titlePart. ' ';
        }
      }
    }

  include( locate_template( 'assets/vc_elements/templates/blue_block.php')); 
});

//SECTION 11
vc_map(
    array(
        'name'          => __('Оранжевый блок', 'wpcentral'),
        'base'          => __('orange_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Название секции', 'wpcentral'),
                                'param_name'    => 'section_title',
                                'value'         => __('Раздел', 'wpcentral'),
                                'admin_label' => true,
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Основной текст', 'wpcentral'),
                                'admin_label' => true,
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на кнопке', 'wpcentral'),
                                'param_name'    => 'btn_text',
                                'value'         => __('Подробней', 'wpcentral'),
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Ссылка', 'wpcentral'),
                                'param_name'    => 'btn_link',
                                'value'         => __('', 'wpcentral'),
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 1', 'wpcentral'),
                                'param_name'    => 'img_1',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 2', 'wpcentral'),
                                'param_name'    => 'img_2',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 3', 'wpcentral'),
                                'param_name'    => 'img_3',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 4', 'wpcentral'),
                                'param_name'    => 'img_4',
                                'group'         => 'Картинки'
                            ),
                            array(
                                'type'          => 'attach_image',
                                'heading'       => __('Картинка 5', 'wpcentral'),
                                'param_name'    => 'img_5',
                                'group'         => 'Картинки'
                            )
                        )
    )
);

add_shortcode('orange_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'section_title' => 'Раздел',
    'main_title'    => 'Основной текст',
    'img_1'         => '',
    'img_2'         => '',
    'img_3'         => '',
    'img_4'         => '',
    'img_5'         => '',
    'btn_text'     => 'Подробней',
    'btn_link'     => '/more'
  ), $atts ) );

    if(!empty($main_title)){
      $title = explode(' ', $main_title);
      $main_title_1 = '';
      $main_title_2 = '';
      foreach($title as $index => $titlePart){
        if($index > 7){
            $main_title_2 .= $titlePart. ' ';
        }else{
            $main_title_1 .= $titlePart. ' ';
        }
      }
    }

  include( locate_template( 'assets/vc_elements/templates/orange_block.php')); 
});

//SECTION 12
vc_map(
    array(
        'name'          => __('Зеленый блок', 'wpcentral'),
        'base'          => __('green_block', 'wpcentral'),
        'category'      => __('Дополнительные компоненты', 'wpcentral'), 
        'params'        => array(
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Название секции', 'wpcentral'),
                                'param_name'    => 'section_title',
                                'value'         => __('Раздел', 'wpcentral'),
                                'admin_label' => true,
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Основной текст', 'wpcentral'),
                                'param_name'    => 'main_title',
                                'value'         => __('Основной текст', 'wpcentral'),
                                'admin_label' => true,
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Текст на кнопке', 'wpcentral'),
                                'param_name'    => 'btn_text',
                                'value'         => __('Подробней', 'wpcentral'),
                                'group'         => 'Основные'
                            ),
                            array(
                                'type'          => 'textfield',
                                'heading'       => __('Ссылка', 'wpcentral'),
                                'param_name'    => 'btn_link',
                                'value'         => __('', 'wpcentral'),
                                'group'         => 'Основные'
                            )
                        )
    )
);

add_shortcode('green_block', function( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'section_title' => 'Раздел',
    'main_title'    => 'Основной текст',
    'btn_text'     => 'Подробней',
    'btn_link'     => '/more'
  ), $atts ) );

  include( locate_template( 'assets/vc_elements/templates/green_block.php')); 
});