<section class="main">
    <video id="bgvid" class="video-bg" playsinline autoplay muted loop>
        <source src="<?php echo get_stylesheet_directory_uri(). '/img/video-bg.mp4'?>" type="video/mp4">
    </video>
    <div class="header-main">
        <h1><?php echo $main_title; ?></h1>
        <h3><?php echo $secondary_title; ?></h3>
    </div>
    <div class="header-a"></div>
</section>