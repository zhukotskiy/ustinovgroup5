<section class="important-section jobs-list light">
    <h5><?php echo $main_title; ?></h5>
    <h2><?php echo $secondary_title; ?></h2>
    <div class="container-fluid">
        <div id="projects-listing" class="row">
            <?php
                $args = array(
                    'post_type'         => 'holding_companies',
                    'post_status'       => 'publish',
                    'orderby'           => 'date',
                    'order'             => 'DESC',
                    'posts_per_page'    => $cards_count,
                );
                $query = new WP_Query;
                $posts = $query->query($args);
                if(!empty($posts)){
                    foreach($posts as $index => $post){
                        $animationDelay = 0.2 + ($index * 0.1);
                        include get_template_directory(). '/assets/vc_elements/templates/projects__loop.php';
                    }
                }
            ?>
        </div>
        <div class="text-center">
            <a href="javascript:void(0);" id="more-projects" class="my-btn-blue new-btn"><?php echo $btn_text; ?></a>
        </div>
        <input type="hidden" id="projects-count" value="<?php echo $cards_count; ?>">
    </div>
</section>
<script>
    $ = jQuery;
    var ajaxUrl = <?php echo '\''. admin_url('admin-ajax.php'). '\';'; ?>

    $(document).on('click', '#more-projects', function(){
        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            async: true,
            data: {
                action: 'more_holding_projects',
                count: $('#projects-count').val()
            }
        }).done(function(response){
            console.log(response);
            $('#projects-listing').append(response);
        });
    })
</script>