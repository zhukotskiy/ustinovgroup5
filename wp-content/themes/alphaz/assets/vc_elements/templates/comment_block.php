<section class="comment-box2">
    <div class="comment-box">
        <div class="box-img">
            <div class="bg-img-prev--round" style="background-image: url(<?php echo (!empty($img)) ? wp_get_attachment_image_url($img) : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
        </div>
        <div class="name-user"><?php echo $name; ?></div>
        <div class="worker"><?php echo $position; ?></div>
        <div class="text-other"><p><?php echo $comment; ?></p></div>
        <a href="<?php echo $btn_link; ?>" class="consult-n"><?php echo $btn_text; ?></a>
    </div>
</section>