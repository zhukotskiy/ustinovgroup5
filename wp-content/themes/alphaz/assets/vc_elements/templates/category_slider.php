<?php
    $queryArgs = array(
        'taxonomy'      => 'holding_companies_cat',
        'parent'        => 0,
        'hide_empty'    => false
    );

    $categories = get_terms($queryArgs);
?>

<?php if(!empty($categories)): ?>
<section class="important-section pt10 light">
    <h2><?php echo $main_title; ?></h2>
    <div class="container-fluid">
        <div id="category_slider" class="row portfolio">
            <?php foreach($categories as $index => $category): ?>
                <div class="col-lg-4 col-md-4 slider-item">
                    <p class="title-image"><?php echo $category->name?></p>
                    <?php
                        $catImage = category_image_src(array('term_id' =>$category->term_id, 'size' => 'full'), false);
                    ?>
                    <div class="bg-img-prev" style="background-image: url(<?php echo (!empty($catImage)) ? $catImage : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
                    <?php if($index - intval($index/3) * 3 == 2): ?>
                    <a href="javascript:void(0);" class="circle-button yellow" data-toggle="modal" data-target="#modal-type">
                        <img src="<?php echo get_stylesheet_directory_uri(). '/img/arrow.png'; ?>">
                    </a>
                    <?php elseif($index - intval($index/3) * 3 == 1): ?>
                    <a href="javascript:void(0);" class="circle-button blue" data-toggle="modal" data-target="#modal-type">
                        <img src="<?php echo get_stylesheet_directory_uri(). '/img/arrow.png'; ?>">
                    </a>
                    <?php elseif($index - intval($index/3) * 3 == 0): ?>
                    <a href="javascript:void(0);" class="circle-button green" data-toggle="modal" data-target="#modal-type">
                        <img src="<?php echo get_stylesheet_directory_uri(). '/img/arrow.png'; ?>">
                    </a>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>