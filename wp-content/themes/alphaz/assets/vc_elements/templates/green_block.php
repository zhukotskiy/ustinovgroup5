<section class="overflow">
    <div class="bg-div">
        <h1 class="development development-second marketing"><?php echo $section_title; ?></h1>
        <img class="line" src="<?php echo get_stylesheet_directory_uri(). '/img/line.png'; ?>">
        <img class="marketing-img" src="<?php echo get_stylesheet_directory_uri(). '/img/bg-marketing.png'; ?>">
        <div class="shape shape1">
            <p class="top-dott">85</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott.png'; ?>">
        </div>
        <div class="shape shape2">
            <p class="top-dott">79</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott.png'; ?>">
        </div>
        <div class="shape shape3">
            <p class="top-dott">55</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott.png'; ?>">
        </div>
        <div class="shape shape4">
            <p class="top-dott">69</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott.png'; ?>">
        </div>
        <div class="shape shape5">
            <p class="top-dott">138</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott2.png'; ?>">
        </div>
        <div class="shape shape6">
            <p class="top-dott">156</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott2.png'; ?>">
        </div>
        <div class="shape shape7">
            <p class="top-dott">143</p>
            <img class="dott1" src="<?php echo get_stylesheet_directory_uri(). '/img/dott2.png'; ?>">
        </div>
        <div class="div-on-image">
            <p><?php echo $main_title; ?></p>
            <a href="<?php echo $btn_link; ?>" class="btn-grey btn-grey-third new-btn"><?php echo $btn_text; ?></a>
        </div>
    </div>
</section>