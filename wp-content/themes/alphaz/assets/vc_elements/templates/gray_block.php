<div class="business-task">
    <div class="block-grey grey2">
        <div class="center-bl">
            <div class="antvan-text wow bounceInLeft" data-wow-duration="1.7s" data-wow-delay="0.9s"><span><?php echo $bg_text; ?></span></div>
            <div class="caption"><?php echo $section_name; ?></div>
            <div class="article-bold">
                <span class="wow fadeInDown" data-wow-duration="0.7s" data-wow-delay="0.5s"><?php echo $main_title; ?></span>
            </div>
            <div class="text-ant2 wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.4s"><?php echo $description; ?></div>
        </div>
    </div>
</div>