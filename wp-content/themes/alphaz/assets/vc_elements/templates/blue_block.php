<section>
    <div class="big-div">
        <h1 class="development"><?php echo $section_title; ?></h1>
        <div class="custom-container-second">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 ">
                                <p class="white-text"><?php echo $main_title_1; ?><br> <?php echo $main_title_2; ?></p>
                                <a href="<?php echo $btn_link; ?>" class="btn-grey btn-grey-second new-btn"><?php echo $btn_text; ?></a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="img-prev-small" style="background-image: url(<?php echo (!empty($img_1)) ? wp_get_attachment_image_url($img_1, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="img-prev-small" style="background-image: url(<?php echo (!empty($img_2)) ? wp_get_attachment_image_url($img_2, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
                            </div>
                        </div>
                        <div class="row second-images">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="img-prev-small" style="background-image: url(<?php echo (!empty($img_3)) ? wp_get_attachment_image_url($img_3, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="img-prev-small" style="background-image: url(<?php echo (!empty($img_4)) ? wp_get_attachment_image_url($img_4, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="img-prev-small" style="background-image: url(<?php echo (!empty($img_5)) ? wp_get_attachment_image_url($img_5, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>)"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>