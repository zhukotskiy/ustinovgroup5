<div class="col-lg-3 col-md-4 col-xs-6 cc wow bounceInUp" data-wow-duration="1s" data-wow-delay="<?php echo $animationDelay. 's'; ?>">
    <?php
        $thumbnail_id = get_post_thumbnail_id($post->ID);
        $thumbnail_url = (!empty($thumbnail_id)) ? wp_get_attachment_image_url($thumbnail_id, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png';
    ?>
    <div class="box-img"><img class="experience-img" src="<?php echo $thumbnail_url; ?>"></div>
    <div class="overlay">
        <div class="text"><?php echo $post->post_title; ?></div>
    </div>
    <div class="text-line">
        <?php $linkToSite = str_replace(array('http://', 'https://', 'www.'), '', get_post_meta($post->ID, 'project_link', true)); ?>
        <div class="caption"><a href="<?php echo 'http://'. $linkToSite; ?>" target="_blank"><?php echo $linkToSite; ?></a></div>
        <div class="small-text"><?php echo get_post_meta($post->ID, 'short_description', true); ?></div>
    </div>
</div>