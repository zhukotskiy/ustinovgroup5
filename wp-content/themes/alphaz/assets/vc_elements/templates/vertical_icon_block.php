<section>
    <div class="custom-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 main-icon-c">
                    <img class="main-icon" src="<?php echo get_stylesheet_directory_uri(). '/img/city.png'; ?>">
                    <img class="counter" src="<?php echo get_stylesheet_directory_uri(). '/img/counter-1.png'; ?>">
                </div>
                <div class="col-lg-8 col-md-8">
                    <h4 class="description"><?php echo $title_1; ?></h4>
                    <p class="paragraph"><?php echo $description_1; ?></p>
                </div>
            </div>
            <div class="row row-description">
                <div class="col-lg-8 col-md-8 ">
                    <h4 class="description"><?php echo $title_2; ?></h4>
                    <p class="paragraph"><?php echo $description_2; ?></p>
                </div>
                <div class="col-lg-4 col-md-4 main-icon-c">
                    <img class="main-icon" src="<?php echo get_stylesheet_directory_uri(). '/img/people.png'; ?>">
                    <img class="counter" src="<?php echo get_stylesheet_directory_uri(). '/img/counter-2.png'; ?>">
                </div>
            </div>
            <div class="row row-description">
                <div class="col-lg-4 col-md-4 main-icon-c">
                    <img class="main-icon" src="<?php echo get_stylesheet_directory_uri(). '/img/paper.png'; ?>">
                    <img class="counter" src="<?php echo get_stylesheet_directory_uri(). '/img/counter-3.png'; ?>">
                </div>
                <div class="col-lg-8 col-md-8">
                    <h4 class="description"><?php echo $title_3; ?></h4>
                    <p class="paragraph"><?php echo $description_3; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>