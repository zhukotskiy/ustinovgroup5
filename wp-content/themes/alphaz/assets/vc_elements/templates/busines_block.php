<div class="business-task">
    <div class="block-grey bg-grass3">
        <div class="center-bl">
            <div class="antvan-text wow bounceInLeft" data-wow-duration="1.7s" data-wow-delay="0.9s"><span><?php echo $bg_text; ?></span></div>
            <div class="caption"><?php echo $section_name; ?></div>
            <div class="article-bold">
                <span class="wow fadeInDown" data-wow-duration="0.7s" data-wow-delay="0.5s"><?php echo $main_title; ?></span>
                <?php if(!empty($secondary_title)): ?>
                    <span class="wow fadeInDown" data-wow-duration="1.1s" data-wow-delay="0.4s"><?php echo $secondary_title?></span>
                <?php endif; ?>
            </div>
            <div class="clearfix">
                <div class="one-block wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.2s">
                    <div class="box-img">
                        <i class="business-ico1"></i>
                    </div>
                    <div class="text-l"><?php echo $text_1; ?></div>
                </div>
                <div class="one-block wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.3s">
                    <div class="box-img">
                        <i class="business-ico2"></i>
                    </div>
                    <div class="text-l"><?php echo $text_2; ?></div>
                </div>
                <div class="one-block wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.4s">
                    <div class="box-img">
                        <i class="business-ico3"></i>
                    </div>
                    <div class="text-l"><?php echo $text_3; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>