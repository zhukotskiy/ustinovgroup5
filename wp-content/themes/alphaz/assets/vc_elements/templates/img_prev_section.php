<section class="best-project">
    <h5><?php echo $main_title; ?></h5>
    <h2><?php echo $secondary_title; ?></h2>
    <div class="flex-imac">
        <div class="flex-imac-child">
            <img src="<?php echo (!empty($img_1)) ? wp_get_attachment_image_url($img_1, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInRightBig" data-wow-duration="2.5s" data-wow-delay="0.1s">
            <img src="<?php echo (!empty($img_2)) ? wp_get_attachment_image_url($img_2, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInRightBig" data-wow-duration="3s" data-wow-delay="0.1s">
            <img src="<?php echo (!empty($img_3)) ? wp_get_attachment_image_url($img_3, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInRightBig" data-wow-duration="2.5s" data-wow-delay="0.1s">
            <img src="<?php echo (!empty($img_4)) ? wp_get_attachment_image_url($img_4, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInRightBig" data-wow-duration="3s" data-wow-delay="0.1s">
        </div>
        <div class="flex-imac-child-main">
            <img class="imac" src="<?php echo get_stylesheet_directory_uri(). '/img/elements.png'; ?>">
            <img class="imac-content" src="<?php echo get_stylesheet_directory_uri(). '/img/PUT&#32;YOUR&#32;DESIGN&#32;HERE.png' ?>">
        </div>
        <div class="flex-imac-child-second">
            <img src="<?php echo (!empty($img_5)) ? wp_get_attachment_image_url($img_5, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInLeftBig" data-wow-duration="3s" data-wow-delay="0.1s">
            <img src="<?php echo (!empty($img_6)) ? wp_get_attachment_image_url($img_6, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInLeftBig" data-wow-duration="2.5s" data-wow-delay="0.1s">
            <img src="<?php echo (!empty($img_7)) ? wp_get_attachment_image_url($img_7, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInLeftBig" data-wow-duration="3s" data-wow-delay="0.1s">
            <img src="<?php echo (!empty($img_8)) ? wp_get_attachment_image_url($img_8, 'large') : get_stylesheet_directory_uri(). '/img/no-image.png'; ?>" class="wow fadeInLeftBig" data-wow-duration="2.5s" data-wow-delay="0.1s">
        </div>
    </div>
</section>