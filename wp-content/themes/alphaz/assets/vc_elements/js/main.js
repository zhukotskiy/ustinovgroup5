$ = jQuery;

new WOW().init();

//category slider
var categorySliderLength = $('#category_slider .slider-item').length;
if(categorySliderLength > 3){
	$('#category_slider').slick({
	    arrows: false,
	    slidesToShow: 3,
	    autoplay: true,
	    autoplaySpeed: 3000,
	    pauseOnFocus: false,
	    responsive: [
	        {
	            breakpoint: 1000,
	            settings: {
	                slidesToShow: 2
	            }
	        },
	        {
	            breakpoint: 600,
	            settings: {
	                slidesToShow: 1
	            }
	        }
	    ]
	});
}

$(document).ready(function(){
	if($('#places-map').length != 0){
        var controller = new ScrollMagic.Controller();
        new ScrollMagic.Scene({
            triggerElement: '#places-map',
            offset: -100
        })
            .on('enter', function(){$('.map-location-marker').addClass('animated bounceInDown');})
            .addTo(controller);
	}
});

// $(function(){
// 	const destination = $(window).height();
// 	  $(window).bind('mousewheel DOMMouseScroll MozMousePixelScroll', function(event) {
// 	    delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
// 	    if (getBodyScrollTop()==0&&delta < 0){
// 			$('body, html').animate({ scrollTop: destination }, 900);
// 			return false;
// 		} else {
// 			localStorage.beforeScroll = getBodyScrollTop();
// 		}
// 	  });

// 	new WOW().init();
// 	$('#js-form').on('click', function(e) {
// 		e.preventDefault();
// 		var element = $(this),
// 			name = $('.modal-body .inputs input[name="name"]').val(),
// 			phone = $('.modal-body .inputs input[name="phone"]').val(),
// 			error = false;
// 		if(!name) {
// 			error = true;
// 			$('.modal-body .inputs input[name="name"]').css('border', '1px solid red');
// 			setTimeout(function(){
// 				error = false;
// 				$('.modal-body .inputs input[name="name"]').css('border', '1px solid #dadada');
// 			}, 2000)
// 		}
// 		if(!phone) {
// 			error = true;
// 			$('.modal-body .inputs input[name="phone"]').css('border', '1px solid red');
// 			setTimeout(function(){
// 				error = false;
// 				$('.modal-body .inputs input[name="phone"]').css('border', '1px solid #dadada');
// 			}, 2000)
// 		}
// 		if(error) {
// 			console.log('Throw new Exeption.');
// 		} else {
// 			$.ajax({
// 				url: 'sender.php',
// 				type: 'POST',
// 				data: {
// 					name: name,
// 					phone: phone
// 				},
// 				dataType: "json",
// 				success: function(){
// 					console.log('success');
// 				}
// 			});
// 		}
// 	});

// 	// var vid = document.getElementById("bgvid");
// 	// var pauseButton = document.querySelector("#polina button");

// 	// if (window.matchMedia('(prefers-reduced-motion)').matches) {
// 	// 	vid.removeAttribute("autoplay");
// 	// 	vid.pause();
// 	// 	pauseButton.innerHTML = "Paused";
// 	// }

// 	// function vidFade() {
// 	// 	vid.classList.add("stopfade");
// 	// }

// 	// vid.addEventListener('ended', function()
// 	// {
// 	// 	vid.pause();
// 	// 	vidFade();
// 	// });


// 	// pauseButton.addEventListener("click", function() {
// 	// 	vid.classList.toggle("stopfade");
// 	// 	if (vid.paused) {
// 	// 		vid.play();
// 	// 		pauseButton.innerHTML = "Pause";
// 	// 	} else {
// 	// 		vid.pause();
// 	// 		pauseButton.innerHTML = "Paused";
// 	// 	}
// 	// })


// });