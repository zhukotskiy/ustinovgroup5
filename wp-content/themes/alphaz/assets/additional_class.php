<?php
class Additional{
	public function get_pager(){
		global $wp_query;
		$pages = '';

		$max = $wp_query->max_num_pages;
		if (!$current = get_query_var('paged')) $current = 1;
		$a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
		$a['total'] = $max;
		$a['current'] = $current;

		$total = 0;
		$a['mid_size'] = 3;
		$a['end_size'] = 1;
		$a['prev_text'] = '<span class="lnr lnr-chevron-left"></span>';
		$a['next_text'] = '<span class="lnr lnr-chevron-right"></span>';

		if ($max > 1) echo '<div class="paged-nav">';
		if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
		echo $pages . paginate_links($a);
		if ($max > 1) echo '</div>';
	}

}

class SearchActions{
	public $search_text = '';

	public function __construct(){
		$this->search_text = $_GET['search_text'];
	}

	public function get_search_text(){

		return $this->search_text;
	}

	public function get_search_result(){
		$search_results = null;

		$args = array(
			'post_type'         => 'projects',
			'post_status'       => 'publish',
			'orderby'           => 'date',
			'order'             => 'DESC',
			'posts_per_page'    => 20,
			's' 				=> $this->search_text
		);

		$query = new WP_Query;
		$search_results = $query->query($args);

		return $search_results;
	}

}