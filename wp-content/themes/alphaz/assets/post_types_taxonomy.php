<?php

//REGISTER POST TYPES
add_action( 'init', function(){

	register_post_type('partners', array(
		'label' => 'Партнеры',
		'labels' => array(
			'name' => 'Партнеры',
			'singular_name' => 'Партнеры',
			'menu_name' => 'Партнеры',
			'all_items' => 'Все',
			'add_new' => 'Новый партнер',
			'add_new_item' => 'Добавить партнера',
			'edit' => 'Редактировать',
			'edit_item' => 'Редактировать',
		),
		'description' => '',
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_rest' => false,
		'rest_base' => '',
		'show_in_menu' => true,
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'has_archive' => false,
		'query_var' => true,
		'supports' => array('title', 'thumbnail')
	));

	register_post_type('projects', array(
		'label' => 'Проекты',
		'labels' => array(
			'name' => 'Проекты',
			'singular_name' => 'Проекты',
			'menu_name' => 'Проекты',
			'all_items' => 'Все',
			'add_new' => 'Новый',
			'add_new_item' => 'Добавить проект',
			'edit' => 'Редактировать',
			'edit_item' => 'Редактировать',
		),
		'description' => '',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_rest' => false,
		'rest_base' => '',
		'show_in_menu' => true,
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'has_archive' => true,
		'query_var' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	));

	register_post_type('holding_companies', array(
		'label' => 'Компании Холдинга',
		'labels' => array(
			'name' => 'Компании Холдинга',
			'singular_name' => 'Компании Холдинга',
			'menu_name' => 'Компании Холдинга',
			'all_items' => 'Все',
			'add_new' => 'Добавить',
			'add_new_item' => 'Добавить',
			'edit' => 'Редактировать',
			'edit_item' => 'Редактировать',
		),
		'description' => '',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_rest' => false,
		'rest_base' => '',
		'show_in_menu' => true,
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'has_archive' => false,
		'query_var' => true,
		'supports' => array('title', 'thumbnail')
	));
});

//REGISTER TAXONOMY
add_action('init', function(){
	register_taxonomy('projects_cat', array('projects'), array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Категория',
			'singular_name'     => 'Категория',
			'search_items'      => 'Поиск',
			'all_items'         => 'Все',
			'view_item '        => 'Просмотр',
			'edit_item'         => 'Редактировать',
			'update_item'       => 'Обновить',
			'add_new_item'      => 'Добавить',
			'new_item_name'     => 'Новая',
			'menu_name'         => 'Категория',
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true,
		'rewrite'               => true,
	) );
	register_taxonomy('projects_type', array('projects'), array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Тип проекта',
			'singular_name'     => 'Тип проекта',
			'search_items'      => 'Поиск',
			'all_items'         => 'Все',
			'view_item '        => 'Просмотр',
			'edit_item'         => 'Редактировать',
			'update_item'       => 'Обновить',
			'add_new_item'      => 'Добавить',
			'new_item_name'     => 'Новый',
			'menu_name'         => 'Тип проекта',
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true,
		'rewrite'               => true,
	));
	register_taxonomy('region', array('projects'), array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Регион',
			'singular_name'     => 'Регион',
			'search_items'      => 'Поиск',
			'all_items'         => 'Все',
			'view_item '        => 'Просмотр',
			'edit_item'         => 'Редактировать',
			'update_item'       => 'Обновить',
			'add_new_item'      => 'Добавить',
			'new_item_name'     => 'Новый',
			'menu_name'         => 'Регион',
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true,
		'rewrite'               => true,
	));
	register_taxonomy('holding_companies_cat', array('holding_companies'), array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Категория',
			'singular_name'     => 'Категория',
			'search_items'      => 'Поиск',
			'all_items'         => 'Все',
			'view_item '        => 'Просмотр',
			'edit_item'         => 'Редактировать',
			'update_item'       => 'Обновить',
			'add_new_item'      => 'Добавить',
			'new_item_name'     => 'Новая',
			'menu_name'         => 'Категория',
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true,
		'rewrite'               => true,
	));
});