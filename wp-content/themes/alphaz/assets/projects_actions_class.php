<?php

class Projects_Actions{
	private static $user;

	public function __construct(){
		self::$user = wp_get_current_user();
	}

	public static function get_projects_count(){
		$args = array(
			'post_type'         => 'projects',
			'post_status'       => 'publish',
			'posts_per_page'    => -1,
			'meta_query' => array(
				array(
					'key' => 'project_end_status',
					'value' => '1',
					'compare' => 'NOT IN'
				)
			)
		);

		$query = new WP_Query;
		$posts = $query->query($args);

		return count($posts);
	}

	public static function get_startup_count(){
		$args = array(
			'post_type'         => 'projects',
			'post_status'       => 'publish',
			'posts_per_page'    => -1,
			'meta_query' => array(
				array(
					'key' => 'project_end_status',
					'value' => '1',
					'compare' => 'NOT IN'
				),
				array(
					'key' => 'project_type',
					'value' => 'type_2'
				)
			)
		);

		$query = new WP_Query;
		$posts = $query->query($args);

		return count($posts);
	}


}