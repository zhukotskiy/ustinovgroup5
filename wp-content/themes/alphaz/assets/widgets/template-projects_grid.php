<section class="light" data-project_type="<?php echo $term_slug; ?>" data-home_filters data-items_count="<?php echo $posts_per_page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
                    <a href="<?php echo site_url(). '/projects_type/'. $term_slug; ?>"><?php echo $title; ?></a>
                </h1>
			</div>
		</div>
<!--        <div class="row pb-4">-->
<!--            <div class="col-12 filters-section d-flex flex-wrap">-->
<!--                <div class="col-lg-3 mt-2 mt-lg-0">-->
<!--                    --><?php
//                        $args = array(
//                            'taxonomy' => 'projects_cat',
//                            'hide_empty' => true,
//                        );
//                        $project_cat = get_terms($args);
//                        $project_cat_arr = get_projects_cat_by_project_type($term_slug);
//                    ?>
<!--                    <select data-filter_type="projects_cat">-->
<!--                        <option value="">Категория</option>-->
<!--                        --><?php //if(!empty($project_cat)): ?>
<!--                            --><?php //foreach($project_cat as $item): ?>
<!---->
<!--                                --><?php //if(in_array($item->slug, $project_cat_arr)): ?>
<!--                                    <option value="--><?php //echo $item->slug; ?><!--">--><?php //echo $item->name; ?><!--</option>-->
<!--                                --><?php //endif; ?>
<!---->
<!--                            --><?php //endforeach; ?>
<!--                        --><?php //endif; ?>
<!--                    </select>-->
<!--                </div>-->
<!--                <div class="col-lg-3 mt-2 mt-lg-0">-->
<!--		            --><?php
//                        $args = array(
//                            'taxonomy' => 'region',
//                            'hide_empty' => true,
//                        );
//                        $project_cat = get_terms($args);
//                        $project_cat_arr = get_projects_region_by_project_type($term_slug);
//		            ?>
<!--                    <select data-filter_type="region">-->
<!--                        <option value="">Регион</option>-->
<!--			            --><?php //if(!empty($project_cat)): ?>
<!--				            --><?php //foreach($project_cat as $item): ?>
<!---->
<!--					            --><?php //if(in_array($item->slug, $project_cat_arr)): ?>
<!--                                    <option value="--><?php //echo $item->slug; ?><!--">--><?php //echo $item->name; ?><!--</option>-->
<!--					            --><?php //endif; ?>
<!---->
<!--				            --><?php //endforeach; ?>
<!--			            --><?php //endif; ?>
<!--                    </select>-->
<!--                </div>-->
<!--                <div class="col-lg-3 mt-2 mt-lg-0">-->
<!--                    <select data-filter_type="projects_price">-->
<!--                        <option value="">Стоимость</option>-->
<!--                        <option value="1">до 10 000$</option>-->
<!--                        <option value="2">10 000$ - 100 000$</option>-->
<!--                        <option value="3">выше 100 000$</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
		<div class="row no-margin items-listing">
			<?php
                foreach($posts as $index => $post){
                    include get_template_directory(). '/parts/loop/project-loop-slider.php';
                }
			?>
		</div>
        <div class="row no-margin mt-2">
            <div class="col-12 d-flex justify-content-center">
                <a href="<?php echo site_url(). '/projects/?fwp_project_type='. $term_slug; ?>" class="more-projects">Больше</a>
            </div>
        </div>
	</div>
</section>