<section class="light">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center"><?php echo $title; ?></h1>
			</div>
		</div>
		<div class="row no-margin fw-projects">
			<?php
				foreach($posts as $index => $post){
					include get_template_directory(). '/parts/loop/project-loop-fw.php';
				}
			?>
		</div>
	</div>
</section>