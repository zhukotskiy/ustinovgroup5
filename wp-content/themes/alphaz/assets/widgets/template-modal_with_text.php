<a href="javascript:void(0)" class="widget-modal-link" data-modal_link="<?php echo $slug?>"><?php echo $title; ?></a>
<div class="modal-window" data-modal_window="<?php echo $slug?>">
	<div class="modal-window-wrapper align-items-start pt-5">
		<span class="close-modal-window lnr lnr-cross"></span>
		<?php $post = get_post($post_id); ?>
		<div class="container description pt-5 pb-5">
			<?php echo apply_filters('the_content', $post->post_content) ?>
		</div>
	</div>
</div>