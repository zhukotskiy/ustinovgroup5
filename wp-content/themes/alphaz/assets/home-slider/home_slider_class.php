<?php


class Home_Slider{
	private $post_id = null;

	public function init(){
		add_action('get_slider', array($this, 'show_slider'));
	}

	public function show_slider($post_id){
		$this->post_id = $post_id;

		$counter_settings = get_post_meta($this->post_id, 'counter_settings', true);
		if($counter_settings == 'show'){
			$counters = $this->get_counters();
		}

		$slides = get_field('slider');
		if(!empty($slides)){
			include( locate_template( 'assets/home-slider/slider-template.php'));

			wp_enqueue_script('tween-max-js');
			wp_enqueue_script('home-slider-js');
		}

	}

	private function get_counters(){
		$real_startup_count = Projects_Actions::get_startup_count();
		$def_startup_count = get_post_meta($this->post_id, 'startup_count', true);
		$output_startup_count = intval($real_startup_count) + intval($def_startup_count);

		$real_project_count = Projects_Actions::get_projects_count();
		$def_projects_count = get_post_meta($this->post_id, 'projects_count', true);
		$output_projects_count = intval($real_project_count) + intval($def_projects_count);

		$real_users_count = Users_Actions::get_all_users_count();
		$def_users_count = get_post_meta($this->post_id, 'register_users_count', true);
		$output_users_count = intval($real_users_count) + intval($def_users_count);


		$real_online_users_count = Users_Actions::get_online_count();
		$def_users_online_count = get_post_meta($this->post_id, 'online_users_count', true);
		$output_users_online_count = intval($real_online_users_count) + intval($def_users_online_count);

		return array(
			'startup_count'         => $output_startup_count,
			'projects_count'        => $output_projects_count,
			'users_count'           => $output_users_count,
			'users_online_count'    => $output_users_online_count
		);

	}
}

$home_slider = new Home_Slider();
$home_slider->init();