<div id="home-slider">

    <!--SLIDES-->
	<div class="slides">
        <?php
            foreach($slides as $slide){
	            include( locate_template( 'assets/home-slider/slider-loop.php'));
            }
        ?>
    </div>

    <!--COUNTERS-->
    <?php if($counter_settings == 'show'): ?>
        <?php include( locate_template( 'assets/home-slider/slider-counters.php')); ?>
    <?php endif; ?>

    <!--NAVIGATION-->
	<div class="slides-nav d-none d-md-block"></div>
</div>