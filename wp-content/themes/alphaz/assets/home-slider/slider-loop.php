<div class="item">
	<div class="item-img">
		<?php
			$img_id = $slide['bg_img'];
			$img_url = (!empty($img_id)) ? wp_get_attachment_image_url($img_id, 'full') : get_stylesheet_directory_uri(). '/img/no-image.png';
		?>
		<div class="item-img-src" style="background-image: url(<?php echo $img_url; ?>)"></div>
	</div>
	<div class="item-description">
		<div class="title"><?php echo $slide['slide_title']; ?></div>
        <div class="link">
            <a href="javascript:void(0)"><?php echo $slide['slide_description']; ?></a>
        </div>
	</div>
</div>