<div id="counters" class="bottom-counters d-lg-flex">
	<?php $page_id = get_option('page_on_front'); ?>
	<div class="counter-item d-none d-lg-block">
		<div class="top">
			<i class="fas fa-briefcase"></i>
			<div id="counters-sect--num_1" class="count" data-count="<?php echo $counters['startup_count']; ?>">0</div>
		</div>
		<div class="description">Стартапов</div>
	</div>
	<div class="counter-item d-none d-lg-block">
		<div class="top">
			<i class="far fa-building"></i>
			<div id="counters-sect--num_2" class="count" data-count="<?php echo $counters['projects_count']; ?>">0</div>
		</div>
		<div class="description">Бизнесов</div>
	</div>
	<div class="counter-item">
		<div class="top">
			<i class="fas fa-users"></i>
			<div id="counters-sect--num_3" class="count" data-count="<?php echo $counters['users_count']; ?>">0</div>
		</div>
		<div class="description">Зарегестрировано</div>
	</div>
	<div class="counter-item">
		<div class="top">
			<i class="fas fa-user"></i>
			<div id="counters-sect--num_4" class="count" data-count="<?php echo $counters['users_online_count']; ?>">0</div>
		</div>
		<div class="description">Сейчас на сайте</div>
	</div>
</div>