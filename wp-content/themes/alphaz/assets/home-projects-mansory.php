<?php

//HOME SLIDER
//add_action('get_home_slider', function(){
//
//	$args = array(
//		'post_type'         => 'home_slider',
//		'post_status'       => 'publish',
//		'orderby'           => 'date',
//		'order'             => 'DESC',
//		'posts_per_page'    => -1,
//	);
//	$query = new WP_Query;
//	$posts = $query->query($args);
//	if(!empty($posts)){
//		include( locate_template( 'assets/home-slider/home-slider.php'));
//	}
//});

//get ended projects
add_action('get_ended_projects', function(){
	$args = array(
		'post_type'         => 'projects',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'meta_query' => array(
			array(
				'key' => 'project_end_status',
				'value' => '1'
			)
		)
	);
	$query = new WP_Query;
	$posts = $query->query($args);
	if(!empty($posts)){
		include get_template_directory(). '/parts/home/ended-projects-slider.php';
	}
});

//get kraudfanding slider
add_action('get-kraudfanding-projects', function(){
	$args = array(
		'post_type'         => 'projects',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'posts_per_page'    => 10,
		'meta_query' => array(
			array(
				'key' => 'project_end_status',
				'value' => '1',
				'compare' => 'NOT IN'
			)

		),
		'tax_query' => array(
			array(
				'taxonomy' => 'projects_type',
				'field' => 'slug',
				'terms' => 'kraudfanding'
			)
		)
	);
	$query = new WP_Query;
	$posts = $query->query($args);


	if(!empty($posts)){
		include get_template_directory(). '/parts/home/kraudfanding-slider.php';
	}
});


//
add_action('get-partners-slider', function(){
	$args = array(
		'post_type'         => 'partners',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'posts_per_page'    => -1,
	);
	$query = new WP_Query;
	$posts = $query->query($args);


	if(!empty($posts)){
		include get_template_directory(). '/parts/home/partners-slider.php';
	}
});