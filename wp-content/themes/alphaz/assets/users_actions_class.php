<?php

class Users_Actions{
	private static $user;

	public function __construct(){
		self::$user = wp_get_current_user();
	}

	public function online_update(){
		if (is_user_logged_in()) {

			$logged_in_users = get_transient('online_status');

			$user = wp_get_current_user();

			$no_need_to_update = isset($logged_in_users[$user->ID]) && $logged_in_users[$user->ID] >  (time() - (1 * 60));

			if(!$no_need_to_update){
				$logged_in_users[$user->ID] = time();
				set_transient('online_status', $logged_in_users, (2*60));
			}
		}
	}

	public function test(){
		add_action('wp', array($this, 'online_update'));
	}

	public static function get_online_count(){
		$logged_in_users = get_transient('online_status');

		return count($logged_in_users);
	}

	public static function get_all_users_count(){
		$users_count = count_users();
		return $users_count['total_users'];
	}

	public static function get_mailbox_link(){

		return site_url(). '/profile/'. self::$user->user_login. '/messages/';
	}

	public static function get_projects_link(){
		return site_url(). '/profile/user-projects';
	}


}

$users_actions = new Users_Actions();
$users_actions->test();