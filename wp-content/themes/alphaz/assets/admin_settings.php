<?php
//THEME SUPPORT
add_theme_support( 'post-thumbnails' );

//REMOVE ADMIN MENU
add_action( 'admin_menu', function() {
	remove_menu_page( 'edit.php' );
	remove_menu_page('edit-comments.php');
});


add_filter('manage_projects_posts_columns', function( $columns ){
	$num = 1;
	$new_columns = array(
	'thumbnail' => 'Привью',
	);

	return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}, 4);

add_filter('manage_projects_posts_columns', function( $columns ){
	$num = 3;
	$new_columns = array(

		'type' => 'Тип проекта',
		'invest_sum' => 'Цена',
		'status' => 'Статус',
		'views' => 'Просмотры',


	);

	return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}, 4);

add_filter('manage_projects_posts_custom_column', function( $colname, $post_id ){
	if( $colname === 'thumbnail' ){
		$thumbnail_id = get_post_thumbnail_id($post_id);
		$thumbnail_url = null;
		if(!empty($thumbnail_id)){
			$thumbnail_url = wp_get_attachment_image_url($thumbnail_id, 'large');
			if(empty($thumbnail_url)) $thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
		}else{
			$thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
		}
		echo '<div style="height: 80px; width: 100%; background-image: url('. $thumbnail_url. '); background-position: center; background-size: cover"></div>';
		//echo '<img src="'. $thumbnail_url. '" style="height: 80px; max-width: 100%">';
	}else if($colname === 'invest_sum' ){
		$invest_sum = get_post_meta($post_id, 'invest_sum', true);
		$currency = get_post_meta($post_id, 'currency', true);
		if($currency == 'usd' || empty($currency)){
			$currency_val = ' $';

		}else if($currency == 'uah'){
			$currency_val = ' грн';

		}else if($currency == 'eur'){
			$currency_val = ' €';

		}

		echo (!empty($invest_sum)) ? $invest_sum. $currency_val: '0 $';


	}else if($colname === 'recommendation'){
		$recommendation = get_post_meta($post_id, 'is_reccomendation', true);
		echo (!empty($recommendation) && $recommendation == '1') ? '<span style="color: #4CAF50; font-weight: 600">Закреплена</span>' : ' - ';
	}else if($colname === 'status' ){
		$project_end_status = get_post_meta($post_id, 'project_end_status', true);
		if($project_end_status == false || empty($project_end_status)){
			$post = get_post($post_id);
			$status =  $post->post_status;

			if($status == 'publish'){
				echo '<span style="color: #2196F2; font-weight: 600">Опубликован</span>';

			}else if($status == 'draft'){
				echo '<span style="color: #9E9E9E; font-weight: 600">Черновик</span>';
			}else{
				echo '<span style="color: #FE5722; font-weight: 600">На проверке</span>';
			}

		}else if($project_end_status == true){
			echo '<span style="color: #4CAF50; font-weight: 600">Завершен</span>';
		}
	}else if($colname === 'views' ) {
		$views = get_post_meta( $post_id, 'views', true );
		echo (!empty($views)) ? $views : '0';
	}else if($colname === 'type' ) {
		$project_type =  get_post_meta($post_id, 'project_type', true);
		if($project_type === 'type_1'){
			echo 'Работающий бизнес';
		}else if($project_type === 'type_2'){
			echo 'Стартап';
		}else if($project_type === 'type_3'){
			echo 'Сбор денег';
		}else if($project_type === 'type_4'){
			echo 'Франшиза';
		}else{
			echo ' - ';
		}
	}
}, 5, 2);

//REGISTER WIDGET AREA (business template)
add_action( 'widgets_init', function(){
	register_sidebar(
		array(
			'id' => 'business_main_widget',
			'name' => 'Бизнес страница',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их на страницу бизнес.',
			'before_widget' => '<div id="%1$s" class="side widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
});
//REGISTER WIDGET AREA footer 1/3)
add_action( 'widgets_init', function(){
	register_sidebar(
		array(
			'id' => 'footer_widget_1',
			'name' => 'Подвал 1/3',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в подвал.',
			'before_widget' => '<div id="%1$s" class="side widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
});
//REGISTER WIDGET AREA footer 2/3)
add_action( 'widgets_init', function(){
	register_sidebar(
		array(
			'id' => 'footer_widget_2',
			'name' => 'Подвал 2/3',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в подвал.',
			'before_widget' => '<div id="%1$s" class="side widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
});
//REGISTER WIDGET AREA footer 1/3)
add_action( 'widgets_init', function(){
	register_sidebar(
		array(
			'id' => 'footer_widget_3',
			'name' => 'Подвал 3/3',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в подвал.',
			'before_widget' => '<div id="%1$s" class="side widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
});

//WIDGETS
class ProjectsGridWidget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'projects_grid',
			'Проекты',
			array( 'description' => 'Позволяет вывести проекты на странице по его типу' )
		);
	}

	public function widget( $args, $instance ) {
		$title = $instance['title'];
		$term_slug = $instance['term_slug'];
		$posts_per_page = $instance['posts_per_page'];


		$args = array(
			'numberposts'       => $posts_per_page,
			'post_type'         => 'projects',
			'post_status'       => 'publish',
			'orderby'           => 'date',
			'order'             => 'DESC',
            'projects_type'     => $term_slug
		);

		$posts = get_posts($args);

		if(!empty($posts)){
			include get_template_directory(). '/assets/widgets/template-projects_grid.php';
		}
	}

	public function form( $instance ) {
		if(isset( $instance[ 'title' ])) $title = $instance['title'];
		if(isset( $instance[ 'term_slug' ])) $term_slug = $instance['term_slug'];
		if(isset( $instance[ 'posts_per_page' ])) $posts_per_page = $instance['posts_per_page'];
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Заголовок</label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>"/>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('term_slug'); ?>">Тип проекта (слаг)</label>
				<input class="widefat" id="<?php echo $this->get_field_id('term_slug'); ?>" name="<?php echo $this->get_field_name('term_slug'); ?>" type="text" value="<?php echo esc_attr($term_slug); ?>"/>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('posts_per_page'); ?>">Количество постов:</label>
				<input id="<?php echo $this->get_field_id('posts_per_page'); ?>" name="<?php echo $this->get_field_name('posts_per_page'); ?>" type="text" value="<?php echo ($posts_per_page) ? esc_attr( $posts_per_page ) : '5'; ?>"/>
			</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['term_slug'] = (!empty($new_instance['term_slug'])) ? strip_tags($new_instance['term_slug']) : '';
		$instance['posts_per_page'] = (is_numeric($new_instance['posts_per_page'])) ? $new_instance['posts_per_page'] : '5';
		return $instance;
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'ProjectsGridWidget' );
});

class FullWidthProjectsSlider extends WP_Widget {
	function __construct() {
		parent::__construct(
			'full_width_projects_slider',
			'Слайдер проектов (на всю ширину)',
			array( 'description' => 'Позволяет вывести проекты на странице по его типу' )
		);
	}

	public function widget( $args, $instance ) {
		$title = $instance['title'];
		$term_slug = $instance['term_slug'];
		$posts_per_page = $instance['posts_per_page'];

		echo $args['before_widget'];

		$args = array(
			'post_type'         => 'projects',
			'post_status'       => 'publish',
			'orderby'           => 'date',
			'order'             => 'DESC',
			'posts_per_page'    => $posts_per_page,
			'meta_query' => array(
				array(
					'key' => 'project_end_status',
					'value' => '1',
					'compare' => 'NOT IN'
				)
			),
			'tax_query' => array(
				array(
					'taxonomy' => 'projects_type',
					'field' => 'slug',
					'terms' => $term_slug
				)
			)
		);
		$query = new WP_Query;
		$posts = $query->query($args);

		if(!empty($posts)){
			include get_template_directory(). '/assets/widgets/template-fw_projects_slider.php';
		}

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		if(isset( $instance[ 'title' ])) $title = $instance['title'];
		if(isset( $instance[ 'term_slug' ])) $term_slug = $instance['term_slug'];
		if(isset( $instance[ 'posts_per_page' ])) $posts_per_page = $instance['posts_per_page'];
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Заголовок</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('term_slug'); ?>">Тип проекта (слаг)</label>
			<input class="widefat" id="<?php echo $this->get_field_id('term_slug'); ?>" name="<?php echo $this->get_field_name('term_slug'); ?>" type="text" value="<?php echo esc_attr($term_slug); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('posts_per_page'); ?>">Количество постов:</label>
			<input id="<?php echo $this->get_field_id('posts_per_page'); ?>" name="<?php echo $this->get_field_name('posts_per_page'); ?>" type="text" value="<?php echo ($posts_per_page) ? esc_attr( $posts_per_page ) : '5'; ?>"/>
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['term_slug'] = (!empty($new_instance['term_slug'])) ? strip_tags($new_instance['term_slug']) : '';
		$instance['posts_per_page'] = (is_numeric($new_instance['posts_per_page'])) ? $new_instance['posts_per_page'] : '5';
		return $instance;
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'FullWidthProjectsSlider' );
});

class ModalWindowWithText extends WP_Widget {
	function __construct() {
		parent::__construct(
			'modal_window_with_text',
			'Модальное окно с текстом',
			array('description' => 'Позволяет вывести ссылку на модальное окно')
		);
	}

	public function widget( $args, $instance ) {
		$title = $instance['title'];
		$slug = $instance['slug'];
		$post_id = $instance['post_id'];

		echo $args['before_widget'];

		include get_template_directory(). '/assets/widgets/template-modal_with_text.php';

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		if(isset( $instance[ 'title' ])) $title = $instance['title'];
		if(isset( $instance[ 'slug' ])) $slug = $instance['slug'];
		if(isset( $instance[ 'post_id' ])) $post_id = $instance['post_id'];
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Заголовок</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('slug'); ?>">Слаг</label>
            <input class="widefat" id="<?php echo $this->get_field_id('slug'); ?>" name="<?php echo $this->get_field_name('slug'); ?>" type="text" value="<?php echo esc_attr($slug); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>">Номер поста:</label>
            <input class="widefat" id="<?php echo $this->get_field_id('post_id'); ?>" name="<?php echo $this->get_field_name('post_id'); ?>" type="post_id" value="<?php echo esc_attr($post_id); ?>"/>
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['slug'] = (!empty($new_instance['slug'])) ? strip_tags($new_instance['slug']) : '';
		$instance['post_id'] = (!empty($new_instance['post_id'])) ? strip_tags($new_instance['post_id']) : '';
		return $instance;
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'ModalWindowWithText' );
});

class SectionNameWidget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'text_widget',
			'Заголовок секции',
			array('description' => 'Позволяет вывести заголовок секции')
		);
	}

	public function widget( $args, $instance ) {
		$title = $instance['title'];

		echo $args['before_widget'];

		echo '<h3 class="widget-title text-center text-lg-left">'. $title. '</h3>';

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		if(isset( $instance[ 'title' ])) $title = $instance['title'];
		?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>">Текст</label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>"/>
            </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'SectionNameWidget' );
});

class InfoSectionWidget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'info_text_widget',
			'Текстовая информация',
			array('description' => 'Позволяет вывести текстовую информацию')
		);
	}

	public function widget( $args, $instance ) {
		$title = $instance['title'];

		echo $args['before_widget'];

		echo '<div class="text-info-widget text-center text-lg-left">'. $title. '</div>';

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		if(isset( $instance[ 'title' ])) $title = $instance['title'];
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Текст</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>"/>
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'InfoSectionWidget' );
});



//DISABLE DEFAULT WIDGETS
function true_remove_default_widget() {
	unregister_widget('WP_Widget_Archives'); // Архивы
	unregister_widget('WP_Widget_Calendar'); // Календарь
	unregister_widget('WP_Widget_Categories'); // Рубрики
	unregister_widget('WP_Widget_Meta'); // Мета
	unregister_widget('WP_Widget_Pages'); // Страницы
	unregister_widget('WP_Widget_Recent_Comments'); // Свежие комментарии
	unregister_widget('WP_Widget_Recent_Posts'); // Свежие записи
	unregister_widget('WP_Widget_RSS'); // RSS
	unregister_widget('WP_Widget_Search'); // Поиск
	unregister_widget('WP_Widget_Tag_Cloud'); // Облако меток
	unregister_widget('WP_Widget_Text'); // Текст
	//unregister_widget('WP_Nav_Menu_Widget'); // Произвольное меню
}

add_action( 'widgets_init', 'true_remove_default_widget', 20 );