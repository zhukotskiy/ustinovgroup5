<?php
/**
 *template name: Business Page
 **/
?>
<?php
    wp_enqueue_style('slick-css');

    //HEADER
    get_header();
?>

<?php do_action('get_slider', $post->ID); ?>

<?php
    $args = array(
        'taxonomy' => 'projects_type',
        'hide_empty' => true,
    );
    $project_type = get_terms($args);
?>
<?php if(!empty($project_type)): ?>
    <section class="light pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center">Тип проекта</h1>
                </div>
            </div>
            <div id="project_categories" class="row justify-content-center">
				<?php foreach($project_type as $item): ?>
                    <div class="col-md-4 col-lg-4 col-xl-3">
                        <a href="<?php echo get_home_url(). '/projects/?fwp_project_type='. $item->slug; ?>" class="home-category mb-1 mt-1 mb-md-2 mt-md-3">
                                    <span class="home-category-item">
                                        <span class="front">
                                            <?php $color = get_field('color', $item); ?>
	                                        <?php $color = (!empty($color)) ? $color : '#1d9d73'; ?>
                                            <span class="icon" style="background: <?php echo $color; ?>">
                                                <?php  $icon_url = category_image_src(array('term_id' =>$item->term_id, 'size' => 'full'), false); ?>
		                                        <?php if(!empty($icon_url)): ?>
                                                    <img src="<?php echo $icon_url; ?>">
		                                        <?php endif; ?>
                                            </span>
                                            <span class="name mt-md-3 pl-3 pl-md-2 pr-2 text-md-center"><?php echo $item->name; ?></span>
                                        </span>
                                        <span class="back" style="background: <?php echo $color; ?>">
                                            <span class="count"><?php echo $item->count. ' Обьявлений'; ?></span>
                                        </span>
                                    </span>
                        </a>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<!--SLIDER-->
<!--<section class="light">-->
<!--    <div class="container pt-4 pb-4">-->
<!--        <div id="pluses-cards" class="row">-->
<!--            <div class="col-md-6 col-lg-4 mt-1 mt-lg-4 ">-->
<!--                <div class="item">-->
<!--                    <div class="img-prev">-->
<!--                        --><?php //include get_stylesheet_directory(). '/img/icons/icon-plus_1.php'?>
<!--                    </div>-->
<!--                    <div class="title">Объединяем инвесторов</div class="title">-->
<!--                    <div class="description">Представителей бизнеса,  венчурных капиталистов, а также владельцев  малого и  крупного бизнеса с целью получения прибыли.</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 col-lg-4 mt-1 mt-lg-4">-->
<!--                <div class="item">-->
<!--                    <div class="img-prev">-->
<!--	                    --><?php //include get_stylesheet_directory(). '/img/icons/icon-plus_2.php'?>
<!--                    </div>-->
<!--                    <div class="title">Лучшие компании</div>-->
<!--                    <div class="description">От идей к реальности. Мы отбираем самые перспективные предприятия,  самых надежных инвесторов для возможности приумножить свой капитал развивая разные сферы услуг.</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 col-lg-4 mt-1 mt-lg-4">-->
<!--                <div class="item">-->
<!--                    <div class="img-prev">-->
<!--	                    --><?php //include get_stylesheet_directory(). '/img/icons/icon-plus_3.php'?>
<!--                    </div>-->
<!--                    <div class="title">Никаких платежей и комиссий</div>-->
<!--                    <div class="description">Мы не взимаем никаких комиссий ни с суммы ваших инвестиций, ни с полученного вами дохода.</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 col-lg-4 mt-1 mt-lg-4">-->
<!--                <div class="item">-->
<!--                    <div class="img-prev">-->
<!--	                    --><?php //include get_stylesheet_directory(). '/img/icons/icon-plus_4.php'?>
<!--                    </div>-->
<!--                    <div class="title">Защита прав инвесторов</div>-->
<!--                    <div class="description">Получите защиту профессионального уровня. Мы работаем по единым стандартам со всеми компаниями и инвесторами.</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 col-lg-4 mt-1 mt-lg-4">-->
<!--                <div class="item">-->
<!--                    <div class="img-prev">-->
<!--	                    --><?php //include get_stylesheet_directory(). '/img/icons/icon-plus_2.php'?>
<!--                    </div>-->
<!--                    <div class="title">Полный комплекс услуг</div>-->
<!--                    <div class="description">Мы не просто инвестиционная платформа. Наша команда работает на всех стадиях: с документами, финансами, отчетами, как с инвесторами, так и с компаниями.</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 col-lg-4 mt-1 mt-lg-4">-->
<!--                <div class="item">-->
<!--                    <div class="img-prev">-->
<!--	                    --><?php //include get_stylesheet_directory(). '/img/icons/icon-plus_5.php'?>
<!--                    </div>-->
<!--                    <div class="title">Юридическое сопровождение</div>-->
<!--                    <div class="description">Все сделки проходят в строгом соответствии с законодательством, обеспечивают максимальную прозрачность и защиту как для инвесторов, так и для предпринимателей.</div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->


<!--<section class="bg-light">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-12">-->
<!--                <h1 class="text-center">Категории</h1>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!--ENDED PROJECTS-->
<?php //do_action('get_ended_projects'); ?>

<!--<section class="light">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-12 text-center pt-5 pb-3">-->
<!--                <a href="http://antvan.ru">-->
<!--                    <img src="--><?php //echo get_stylesheet_directory_uri(). '/img/antvan-logo.png'?><!--">-->
<!--                </a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->


<?php if (is_active_sidebar('business_main_widget')): ?>
    <?php dynamic_sidebar( 'business_main_widget'); ?>
<?php endif; ?>


<?php do_action('get-partners-slider'); ?>


<?php
    wp_enqueue_script('tween-max-js');
    wp_enqueue_script('slick-slider-js');
    wp_enqueue_script('scroll-magic-js');
    wp_enqueue_script('scroll-magic-animation-js');
    wp_enqueue_script('scroll-magic-indicator-js');
    wp_enqueue_script('home-js');

    //FOOTER
    get_footer();
?>