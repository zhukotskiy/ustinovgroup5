<div id="footer">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="order-2 col-lg-3 col-xl-3 mt-3">
                <!-- <div class="logo text-center text-lg-left"> -->
                    <!-- <img src="<?php //echo get_stylesheet_directory_uri(). '/img/ustinn_logo.jpg'?>" alt=""> -->
                <!-- </div> -->
                <div class="copyright text-center text-lg-left">
                    Copyright © 2019 Ustinov Group</br>
                   All rights reserved
               </div>
            </div>
            <div class="order-1 col-lg-9 col-xl-9">
                <div class="row">
                    <div class="col-lg-4 mt-3">
	                    <?php if (is_active_sidebar('footer_widget_1')): ?>
		                    <?php dynamic_sidebar( 'footer_widget_1'); ?>
	                    <?php endif; ?>
                    </div>
                    <div class="col-lg-4 mt-3">
	                    <?php if (is_active_sidebar('footer_widget_2')): ?>
		                    <?php dynamic_sidebar( 'footer_widget_2'); ?>
	                    <?php endif; ?>
                    </div>
                    <div class="col-lg-4 mt-3">
	                    <?php if (is_active_sidebar('footer_widget_3')): ?>
		                    <?php dynamic_sidebar( 'footer_widget_3'); ?>
	                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="pre-loader">
    <div class="site-name">USTINOV</div>
    <div id="loader"></div>
</div>
<div class="modal-window" data-modal_window="search">
    <div class="modal-window-wrapper">
        <span class="close-modal-window lnr lnr-cross"></span>
        <form action="<?php echo site_url(). '/search-page'; ?>" method="GET" class="search-form">
            <input type="text" id="search-field" name="search_text" placeholder="Начните вводить текст..." autocomplete="off">
            <label for="search-field">Для продолжения нажмите Enter</label>
        </form>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>