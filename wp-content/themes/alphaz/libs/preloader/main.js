$ = jQuery;
$(window).on('load', function(){

    $('#preloader').delay(400).fadeOut('slow');
    setTimeout(function(){
        $('body').css({'height': 'auto', 'overflow': 'auto'});
    }, 400);

});

$('form').submit(function(){
    $('body').css({'height': 'auto', 'overflow': 'auto'});
    setTimeout(function(){
        $('#preloader').delay(400).fadeIn('slow');
    }, 100);
});