<?php
/*
Template Name: Search
*/
?>

<?php
    get_header();
    $search_actions = new SearchActions();
?>
	<section class="light mt-5 pt-5">
		<div class="container">
			<div class="row">
				<div class="col-12"></div>
			</div>
			<?php if(!empty($search_actions->search_text)): ?>
                <div class="row">
                    <div class="col-12 pt-3 pb-3">
                        <span class="search-title">Результат по запросу:</span>
                        <span class="search-text"><?php echo '"'. $search_actions->search_text. '"'; ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="divider"></div>
                    </div>
                </div>
			<?php endif; ?>
			<?php $search_result = $search_actions->get_search_result(); ?>
            <?php if(!empty($search_result)): ?>
                <div class="row">
                    <?php foreach($search_result as $post): ?>
	                    <?php include( locate_template( 'parts/loop/project-loop-slider.php')); ?>
                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-12">
                        <span>По данному запросу ничего не найдено.</span>
                    </div>
                </div>
            <?php endif; ?>




		</div>
	</section>
<?php get_footer(); ?>


