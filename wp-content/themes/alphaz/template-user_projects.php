<?php
/**
 *template name: User Projects
 **/
?>
<?php
    if(!is_user_logged_in()){
        wp_redirect('/auth');
    }

    get_header();
?>
<section class="light pt-5">
    <div class="container">
        <div class="row">
            <div class="col d-none d-sm-block">
                <h5 class="text-right font-type-2">Рады снова вас видеть, <?php do_action('get_user_name'); ?></h5>
            </div>
            <div class="col d-block d-sm-none">
                <h5 class="text-right font-type-2">Рады снова вас видеть,<br><?php do_action('get_user_name'); ?></h5>
            </div>
        </div>
        <div class="row">
            <div class="col"><h3>Профиль</h3></div>
<!--            <div class="col">-->
<!--                <h3 class="text-right font-type-2 mb-0">$600</h3>-->
<!--                <h6 class="text-right text-caption pt-0 mt-0 font-type-2">на счету</h6>-->
<!--            </div>-->
        </div>
        <div class="row">
            <div class="col-12 navigation d-flex flex-wrap">
                <a href="<?php echo site_url().'/profile/user-projects'?>" class="tab-item active">
                    <span>Мои проекты</span>
                </a>
                <a href="<?php do_action('get_user_messages_url') ?>" class="tab-item <?php echo (bp_is_user_messages()) ? 'active' : '' ;?>">
                    <span>Сообщения</span>
                </a>
                <a href="<?php echo site_url().'/profile/favorite'?>" class="tab-item">
                    <span>Избранное</span>
                </a>
                <a href="<?php do_action('get_user_profile_settings_url') ?>" class="tab-item <?php echo (bp_is_user_settings()) ? 'active' : '' ;?>">
                    <span>Настройки аккаунта</span>
                </a>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="divider"></div>
        </div>
    </div>
</section>
<?php if(is_user_edit_project_page()): ?>
    <section id="user-projects" class="light">
        <div class="container">
			<?php do_action('get_user_edit_project_page'); ?>
        </div>
    </section>
<?php elseif(!empty($_GET['action']) && $_GET['action'] == 'add_new_project'): ?>
    <section id="user-projects" class="light">
        <div class="container">
			<?php do_action('get_user_add_new_project_page'); ?>
        </div>
    </section>
<?php else: ?>
    <section id="user-projects" class="light">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-end pb-3">
                    <a href="<?php echo get_site_url(). '/profile/user-projects?action=add_new_project'; ?>" class="filled-btn">Добавить проект</a>
                </div>
            </div>
			<?php do_action('get_user_projects'); ?>
        </div>
    </section>
<?php endif; ?>


<!-- Footer -->
<?php get_footer(); ?>
