<?php get_header(); ?>
	<section class="light mt-5 pt-2">
		<div class="container">
			<div class="row">
				<div class="col-12 breadcrumbs">
					<a href="<?php echo home_url(); ?>">Главная</a>
					<span class="lnr lnr-chevron-right"></span>
					<a href="javascript:void(0)" class="disabled">Проекты</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3">
					<?php echo facetwp_display( 'facet', 'project_type' ); ?>
				</div>
				<div class="col-lg-3">
					<?php echo facetwp_display( 'facet', 'categories' ); ?>
				</div>
				<div class="col-lg-3">
					<?php echo facetwp_display( 'facet', 'region' ); ?>
				</div>
				<div class="col-lg-3">
					<?php echo facetwp_display( 'facet', 'pproject_price' ); ?>
				</div>
			</div>
			<div class="row facetwp-template">
				<?php if(have_posts()): ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php include( locate_template( 'parts/loop/project-loop-slider.php')); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-12 pt-5 pb-5">
					<?php echo facetwp_display( 'pager' ); ?>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>