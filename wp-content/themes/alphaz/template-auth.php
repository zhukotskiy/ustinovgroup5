<?php
/**
 *template name: Auth
 **/
?>
<?php get_header('empty'); ?>
	<section id="auth">
		<div class="auth-body">
			<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
				<a href="<?php echo get_site_url(); ?>" class="go-home"><span class="lnr lnr-cross "></span></a>
				<div class="logo">USTINN</div>
				<input type="hidden" name="action" value="user_auth">
				<input type="email" name="user_email" class="auth-input" placeholder="Email" required>
				<?php if($_GET['auth_error']): ?>
				<div class="error_message">Направильно введен email или пароль</div>
				<?php endif; ?>
				<input type="text" name="user_pass"  class="auth-input" placeholder="Password" required>
                <button class="auth-submit">Войти</button>
				<div class="create-acc text-center">
					<span>Нет аккаунта?</span>
					<a href="<?php echo get_site_url(). '/register'; ?>">Зарегистрироваться</a>
				</div>
<!--                <div class="divider mt-5"></div>-->
<!--                <div class="create-acc text-center mt-4">-->
<!--                    <span>Забыли пароль?</span>-->
<!--                    <a href="--><?php //echo get_site_url(). '/lost_pass'; ?><!--">Восстановить</a>-->
<!--                </div>-->
			</form>
		</div>
	</section>
<?php get_footer('empty'); ?>
