<?php
//INCLUDES

include get_stylesheet_directory(). '/assets/vc_elements/vc_elements.php';

include get_stylesheet_directory(). '/assets/post_types_taxonomy.php';
include get_stylesheet_directory(). '/assets/admin_settings.php';

include get_stylesheet_directory(). '/assets/home-projects-mansory.php';
include get_stylesheet_directory(). '/assets/home-slider/home_slider_class.php';
include get_stylesheet_directory(). '/assets/additional_class.php';
$additional = new Additional();


include get_stylesheet_directory(). '/assets/projects_actions_class.php';
include get_stylesheet_directory(). '/assets/users_actions_class.php';
include get_stylesheet_directory(). '/buddypress/functions.php';

include get_stylesheet_directory(). '/assets/facet_wp/facetwp-range-list.php';
//add_theme_support( 'buddypress' );

// hide admin bar for all
show_admin_bar(false);

//STYLES & SCRIPTS
//STYLES
wp_register_style('style', get_stylesheet_directory_uri(). '/style.css');
//fonts
wp_register_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
wp_register_style('linear-icons', 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css');
wp_register_style('font-awesome', 'https://use.fontawesome.com/releases/v5.0.6/css/all.css');
wp_register_style('main-css', get_stylesheet_directory_uri(). '/css/main.css');
//home slider
wp_register_style('slick-css', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css');
wp_register_style('home-slider-css', get_stylesheet_directory_uri(). '/css/home-slider.css');
//custom map
wp_register_style('map-css', get_stylesheet_directory_uri(). '/css/map.css');
//light gallery
wp_register_style('lightgallery-css', get_stylesheet_directory_uri(). '/libs/lightgallery/css/lightgallery.css');
//selectric
wp_register_style('selectric', get_stylesheet_directory_uri(). '/libs/selectric/selectric.css');

//SCRIPTS
wp_register_script('loader-js', get_stylesheet_directory_uri() . '/js/loader.js', 'jquery', '1.0.0', TRUE);
wp_register_script('main-js', get_stylesheet_directory_uri() . '/js/main.js', 'jquery', '1.0.0', TRUE);
wp_register_script('sticky-js', get_stylesheet_directory_uri() . '/libs/sticky/sticky.js', 'jquery', '1.0.0', TRUE);
wp_register_script('home-js', get_stylesheet_directory_uri() . '/js/home.js', 'jquery', '1.0.0', TRUE);
//tween max
wp_register_script('tween-max-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('tween-max-css-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/plugins/CSSPlugin.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('tween-max-ease-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/easing/EasePack.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('tween-max-lite-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenLite.min.js', 'jquery', '1.0.0', TRUE);
//slider
wp_register_script('slick-slider-js', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('home-slider-js', get_stylesheet_directory_uri() . '/js/home-slider.js', 'jquery, tween-max-js, slick-slider-js', '1.0.0', TRUE);
//scroll magic
wp_register_script('scroll-magic-js', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.js', 'jquery', '1.0.0', TRUE);
wp_register_script('scroll-magic-animation-js', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js', 'jquery', '1.0.0', TRUE);
wp_register_script('scroll-magic-indicator-js', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', 'jquery', '1.0.0', TRUE);
//map
wp_register_script('map-js', get_stylesheet_directory_uri() . '/js/map.js', 'jquery', '1.0.0', TRUE);
//light gallery
wp_register_script('lightgallery-js', get_stylesheet_directory_uri() . '/libs/lightgallery/lightgallery-all.js', 'jquery', '1.0.0', TRUE);
wp_register_script('jquery_mousewheel-js', get_stylesheet_directory_uri() . '/libs/lightgallery/jquery.mousewheel.min.js', 'jquery', '1.0.0', TRUE);
wp_register_script('picturefill-js', 'https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js', 'jquery', '1.0.0', TRUE);
//profile
wp_register_script('profile-js', get_stylesheet_directory_uri() . '/js/profile.js', 'jquery', '1.0.0', TRUE);
//selectric
wp_register_script('selectric', get_stylesheet_directory_uri() . '/libs/selectric/selectric.min.js', 'jquery', '1.0.0', TRUE);
//home filters
wp_register_script('home-filters', get_stylesheet_directory_uri() . '/js/filters.js', 'jquery', '1.0.0', TRUE);

if(!is_admin()){
    //styles

	wp_enqueue_style('material-icons');
	wp_enqueue_style('linear-icons');
	wp_enqueue_style('font-awesome');
	wp_enqueue_style('main-css');
    wp_enqueue_style('style');
    //scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('sticky-js');
	wp_enqueue_script('main-js');
	wp_enqueue_script('loader-js');
	wp_enqueue_script('home-filters');


//    wp_enqueue_script('slick-js');
//    wp_enqueue_script('wow-js');
//    wp_enqueue_script('main-js');
}

//ADDITIONAL FUNCTIONS
function getPostThumbnail($post_id, $size = 'large'){
	$thumbnail_url = null;
	$thumbnail_id = get_post_thumbnail_id($post_id);
	if(!empty($thumbnail_id)){
		$thumbnail_url = wp_get_attachment_image_url($thumbnail_id, $size);
		if(empty($thumbnail_url)) $thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
	}else{
		$thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
	}

	return $thumbnail_url;
}

// register menu
add_action('after_setup_theme', function(){
	register_nav_menus( array(
		'nav_menu' => 'Меню навигации',
		'footer_menu' => 'Меню подвал'
	));
});

// get header menu
add_action('get_header_menu', function(){
	$navigation_menu = array();

	$locations = get_nav_menu_locations();
	$nav_menu = wp_get_nav_menu_object($locations['nav_menu']);
	$nav_menu_items = wp_get_nav_menu_items($nav_menu->term_id);

	if(!empty($nav_menu_items)){
		foreach($nav_menu_items as $menu_item){
			if ($menu_item->menu_item_parent == 0){
				$navigation_menu[$menu_item->ID] = array(
					'url' => $menu_item->url,
					'name' => $menu_item->title
				);
			}else{
				$navigation_menu[$menu_item->menu_item_parent]['submenu'][] = array(
					'url' => $menu_item->url,
					'name' => $menu_item->title
				);
			}
		}
	}

	$output = '';
	if(!empty($navigation_menu)) {
		$output .=  '<ul>';
			foreach ($navigation_menu as $menu_item){
				$output .= '<li class="'. ( (!empty($menu_item['submenu'])) ? 'has-submenu' : '' ). '">';
				$output .= '<a href="'. $menu_item['url']. '">'. $menu_item['name']. '</a>';
				if(!empty($menu_item['submenu'])){
					$output .= '<ul class="submenu">';

						foreach($menu_item['submenu'] as $submenu_item){
							$output .= '<li class="submenu-item"><a href="'. $submenu_item['url']. '">'. $submenu_item['name']. '</a></li>';
						}

					$output .= '</ul>';
				}
				$output .= '</li>';
			}
		$output .= '</ul>';
	}

	echo $output;
});

function top_nav_output(){
	$navigation_menu = array();
	$locations = get_nav_menu_locations();
	$nav_menu = wp_get_nav_menu_object($locations['nav_menu']);

	$nav_menu_items = wp_get_nav_menu_items( $nav_menu->term_id );

	if(!empty($nav_menu_items)){
		foreach($nav_menu_items as $menu_item){
			if ($menu_item->menu_item_parent == 0){
				$navigation_menu[$menu_item->ID] = array(
					'url' => $menu_item->url,
					'name' => $menu_item->title
				);
			}else{
				$navigation_menu[$menu_item->menu_item_parent]['submenu'][] = array(
					'url' => $menu_item->url,
					'name' => $menu_item->title
				);
			}
		}
	}
}





add_action('get-home-map', function(){
	include get_template_directory(). '/parts/sections/map.php';
	wp_enqueue_script('map-js');
});

//FUNCTIONS
function get_thumbnail($post_id, $size = 'large'){
	$thumbnail_url = '';
	$thumbnail_id = get_post_thumbnail_id($post_id);
	if(!empty($thumbnail_id)){
		$thumbnail_url = wp_get_attachment_image_url($thumbnail_id, $size);
		if(empty($thumbnail_url)) $thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
	}else{
		$thumbnail_url = get_stylesheet_directory_uri(). '/img/no-image.png';
	}

	return $thumbnail_url;
}



//USER PROJECT PAGE
function is_user_edit_project_page(){
	if($_GET['action'] == 'edit'){
		return true;
	}

	return false;
}


function get_styled_post_status($post_status){
	$output = '';

	switch($post_status){
		case 'publish':
			$output = '<span class="text-green">Опубликован</span>';
			break;
		case 'draft':
			$output = '<span class="text-secondary">Черновик</span>';
			break;
		case 'pending':
			$output = '<span class="text-orange">На проверке</span>';
			break;
	}

	return $output;
}

function get_post_favorites_count($post_id){
	return intval(get_favorites_count($post_id)) + intval(get_post_meta($post_id, 'get_phone', true));
}

//ACTIONS
add_action('get_related_posts', function($post_id){

	$cur_project_type = wp_get_post_terms($post_id, 'projects_type', array('fields' => 'all'))[0];

	$args = array(
		'post_type'         => 'projects',
		'post_status'       => 'publish',
		'orderby'           => 'rand',
		'post__not_in' => array($post_id),
		'posts_per_page'    => 4,
		'meta_query' => array(
			array(
				'key' => 'project_end_status',
				'value' => '1',
				'compare' => 'NOT IN'
			)
		),
		'tax_query' => array(
			array(
				'taxonomy' => 'projects_type',
				'field' => 'slug',
				'terms' => $cur_project_type->slug
			)
		)
	);
	$query = new WP_Query;
	$posts = $query->query($args);

	if(!empty($posts)){
		foreach ($posts as $post){
			include get_template_directory(). '/parts/loop/project-loop-slider.php';
		}
	}
});

add_action('get_project_gallery', function(){
	include( locate_template( '/parts/single-project/gallery.php'));
});

add_action('get_user_projects', function(){
	global $current_user;
	$args = array(
		'post_type'         => 'projects',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'author'            =>  $current_user->ID,
		'post_status'       => 'all',
		'posts_per_page'    => -1,

	);
	$query = new WP_Query;
	$posts = $query->query($args);

	if(!empty($posts)){
		foreach ($posts as $post){
			include get_template_directory(). '/parts/user-projects/projects-loop.php';
		}
	}else{
		echo '<h3 class="text-center text-secondary">Добавьте проект</h3>';
	}

	delete_draft_projects();

});

function delete_draft_projects(){
	global $current_user;
	if($current_user->roles[0] == 'subscriber'){
		$args = array(
			'post_type'         => 'projects',
			'orderby'           => 'date',
			'order'             => 'DESC',
			'author'            => $current_user->ID,
			'post_status'       => array('draft'),
			'posts_per_page'    => -1,

		);
		$query = new WP_Query;
		$posts = $query->query($args);
		if(!empty($post)){
			foreach($posts as $post){
				wp_delete_post($post->ID);
			}
		}
	}
}

add_action('get_user_edit_project_page', function(){
	$id = $_GET['project_id'];
	$post = get_post( $id, OBJECT, 'edit' );
	include get_template_directory(). '/parts/user-projects/edit-project.php';
});

add_action('get_user_add_new_project_page', function(){
	include get_template_directory(). '/parts/user-projects/create-project.php';
});



//HOOKS
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method() {
	wp_deregister_script( 'jquery-core' );
	wp_register_script( 'jquery-core', 'https://code.jquery.com/jquery-3.3.1.js');
	wp_enqueue_script( 'jquery' );
}

add_action('save_post_projects', function($post_id){
	$project_sku = get_post_meta($post_id, 'sku', true);

	if(empty($project_sku)){
		$new_sku = 1000 + intval($post_id);
		update_post_meta($post_id, 'sku', $new_sku);
	}
});

add_filter( 'favorites/button/html', function (){

	return '<i class="material-icons">grade</i>';
}, 10, 3);


//USER ADD NEW PROJECT
function user_add_project(){
	$post_id = '';
	$user = wp_get_current_user();

	if(!empty($user)){
		$post_data = array(
			'post_title'  => '',
			'post_status' => 'draft',
			'post_type'   => 'projects',
			'post_author' => $user->ID,
		);
		$post_id = wp_insert_post($post_data);
	}

	return $post_id;
}

add_action( 'admin_post_user_save_post', function(){
	$user = wp_get_current_user();

	if($_POST['post_id'] == '0'){
		$post_id = user_add_project();
	}else{
		$post_id = $_POST['post_id'];
	}

	$post = get_post($post_id);

	if($post->post_author == $user->ID){
		wp_update_post(wp_slash(array(
			'ID'            => $post_id,
			'post_title'    => $_POST['post_title'],
			'post_content'  => $_POST['postedit'],
			'post_status'   => 'publish'

		)));

		wp_set_post_terms($post_id, $_POST['project_type'], 'projects_type', false );
		wp_set_post_terms($post_id, $_POST['projects_cat'], 'projects_cat', false );
		wp_set_post_terms($post_id, $_POST['projects_region'], 'region', false );

		update_post_meta($post_id, 'short_description', $_POST['short_description']);
		update_post_meta($post_id, 'address', $_POST['address']);
		update_post_meta($post_id, 'phone', $_POST['phone']);
		update_post_meta($post_id, 'project_link', $_POST['project_link']);
		update_post_meta($post_id, 'invest_sum', $_POST['invest_sum']);
		update_post_meta($post_id, '_thumbnail_id', $_POST['thumbnail']);
		$gallery = array();
		$gallery_json = $_POST['gallery'];
		if(!empty($gallery_json)){} $gallery = json_decode($gallery_json);
		update_post_meta($post_id, '_gallery', $gallery);
		update_post_meta($post_id, 'currency', $_POST['currency']);
		wp_redirect('/profile/user-projects');
	}else{
		wp_redirect('/');
	}

	exit;
});

add_action( 'admin_post_remove_user_project', function(){
	$user = wp_get_current_user();
	$post_id = $_POST['post_id'];
	$post = get_post($post_id);

	if($post->post_author == $user->ID){
		wp_delete_post($post_id);
		wp_redirect('/profile/user-projects');
	}

	exit;
});

add_action('wp_ajax_remove_post_thumbnail', function(){
	$post_id = $_POST['post_id'];
	$img_id = $_POST['img_id'];

	//update_post_meta( $post_id, '_thumbnail_id', NULL);
	wp_delete_attachment( $img_id, true );

	echo json_encode(array(
		'status' => 'ok'
	));

	wp_die();
});

//add_action('wp_ajax_set_post_thumbnail', function(){
//	$post_id = $_POST['post_id'];
//	$img_id = $_POST['img_id'];
//
//	update_post_meta( $post_id, '_thumbnail_id', $img_id);
//
//	echo json_encode(array(
//		'status' => 'ok'
//	));
//
//	wp_die();
//});

add_action('wp_ajax_upload_new_image_file', function(){
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	$img_id = media_handle_upload( 'image_file', $_POST['post_id']);

	if(is_wp_error($img_id)){
		echo json_encode(array(
			'status' => 'error'
		));
	}else{
		echo json_encode(array(
			'status' => 'ok',
			'img_id' => $img_id,
			'img_url' => wp_get_attachment_image_url($img_id, 'full')
		));
	}

	wp_die();
});

add_action('wp_ajax_remove_img_from_project_gallery', function(){
	$post_id = $_POST['post_id'];
	$img_id = $_POST['img_id'];

	$gallery = get_post_meta( $post_id, '_gallery', true);
	$key = array_search($img_id, $gallery);
	unset($gallery[$key]);
	update_post_meta($post_id, '_gallery', $gallery);

	wp_delete_attachment( $img_id, true );



	echo json_encode(array(
		'status' => 'ok'
	));

	wp_die();
});

add_action('wp_ajax_add_img_to_gallery', function(){
	$post_id = $_POST['post_id'];
	$img_id = $_POST['img_id'];

	$gallery = get_post_meta( $post_id, '_gallery', true);
	if(empty($gallery)) $gallery = array();
	$gallery[] = $img_id;
	update_post_meta($post_id, '_gallery', $gallery);


	echo json_encode(array(
		'status' => 'ok'
	));

	wp_die();
});


add_action('admin_post_send_message_to_author', function(){
	$user = wp_get_current_user();
	$post_id = $_POST['post_id'];
	$message_content = $_POST['message-content'];
	$post = get_post($post_id);

	messages_new_message(
		array(
			'sender_id'  => $user->ID,
			'subject'    => $post->post_title,
			'content'    => $message_content,
			'recipients' => $post->post_author
		)
	);


	wp_redirect('/projects/'. $post->post_name);
	exit;
});

//LOGIN
add_action('admin_post_nopriv_user_auth', function(){
	$user_email = $_POST['user_email'];
	$user_pass = $_POST['user_pass'];

	$user_info = array(
		'user_login'    => $user_email,
		'user_password' => $user_pass,
		'remember'      => true
	);

	$user = wp_signon($user_info, false );

	if(is_wp_error($user)){
		wp_redirect('/auth/?auth_error=true');
	}else{
		wp_redirect('/profile/user-projects');
	}
});

//REGISTER
add_action('admin_post_nopriv_user_register', function(){
	$user_email = $_POST['user_email'];
	$exploded_user_email = explode('@', $user_email)[0];

	$user_nick = str_replace('.', '_', $exploded_user_email);

	$user_pass = $_POST['user_pass'];
	$user_first_name = $_POST['user_first_name'];

	$user_id = wp_create_user($user_nick, $user_pass, $user_email);

	if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $user_email)){
		wp_redirect('/register/?register_error=email_is_incorrect');
	}else{
		if (is_wp_error($user_id)){
			$error_code = $user_id->get_error_code();
			wp_redirect('/register/?register_error='. $error_code);
		}else{

			$user_data = array(
				'ID'              => $user_id,
				'first_name'      => $user_first_name
			);
			wp_update_user(array('ID' => $user_id, 'first_name' => $user_first_name));

			$user_info = array(
				'user_login'    => $user_email,
				'user_password' => $user_pass,
				'remember'      => true
			);
			$user = wp_signon($user_info, false );

			wp_redirect('/profile/'. $user_nick. '/settings');
		}
	}

	exit;
});

//LOST PASS
add_action('admin_post_nopriv_lost_pass', function(){
	wp_redirect('/lost_pass');
});

add_action('wp_ajax_get_home_filtered_items', 'get_home_filtered_items');
add_action('wp_ajax_nopriv_get_home_filtered_items', 'get_home_filtered_items');
function get_home_filtered_items(){
	$items_count = $_POST['items_count'];
	$project_type = $_POST['project_type'];
	$project_region = $_POST['region'];
	$projects_cat = $_POST['projects_cat'];
	$projects_price = $_POST['projects_price'];


	$projects_price_filter = array();
	if(!empty($projects_price)){

		if($projects_price == '1'){

			$projects_price_filter = array(
				'key' => 'invest_sum',
				'value'   => 10000,
				'type'    => 'numeric',
				'compare' => '<'
			);

		}else if($projects_price == '2'){

			$projects_price_filter = array(
				'key' => 'invest_sum',
				'value'   => array(10000, 100000),
				'type'    => 'numeric',
				'compare' => 'BETWEEN'
			);

		}else if($projects_price == '3'){

			$projects_price_filter = array(
				'key' => 'invest_sum',
				'value'   => 100000,
				'type'    => 'numeric',
				'compare' => '>'
			);

		}

	}

	$args = array(
		'numberposts'       => $items_count,
		'post_type'         => 'projects',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'projects_type'     => $project_type,
		'projects_cat'      => $projects_cat,
		'region'            => $project_region,
		'meta_query' => array('relation' => 'AND', $projects_price_filter)
	);

	$posts = get_posts($args);

	if(!empty($posts)){
		foreach($posts as $index => $post){
			include get_template_directory(). '/parts/loop/project-loop-slider.php';
		}
	}else{
		echo '<div class="col-12"><h3 class="text-center pt-5 pb-5">По данному запросу ничего не найдено</h3></div>';
	}

	wp_die();
}

function get_projects_cat_by_project_type($project_type){
	$output = array();

	$args = array(
		'numberposts'       => -1,
		'post_type'         => 'projects',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'projects_type'     => $project_type
	);

	$posts = get_posts($args);

	if(!empty($posts)){
		foreach($posts as $post){
			$terms = get_the_terms($post->ID, 'projects_cat');
			if(!empty($terms)){
				foreach($terms as $term){
					if(!in_array($term->name, $output)){
						$output[] = $term->slug;
					}
				}
			}
		}
	}

	return $output;
};

function get_projects_region_by_project_type($project_type){
	$output = array();

	$args = array(
		'numberposts'       => -1,
		'post_type'         => 'projects',
		'post_status'       => 'publish',
		'orderby'           => 'date',
		'order'             => 'DESC',
		'projects_type'     => $project_type
	);

	$posts = get_posts($args);

	if(!empty($posts)){
		foreach($posts as $post){
			$terms = get_the_terms($post->ID, 'region');
			if(!empty($terms)){
				foreach($terms as $term){
					if(!in_array($term->name, $output)){
						$output[] = $term->slug;
					}
				}
			}
		}
	}

	return $output;
};

add_action( 'save_post_projects', function($post_id){
	$price = get_post_meta($post_id, 'invest_sum', true);
	if($price != 'Договорная' || empty($price)){
		$prepended_price = preg_replace('~\D+~', '', $price);
		update_post_meta($post_id, 'invest_sum', $prepended_price);
	}
}, 10, 3 );

add_action('wp_ajax_set_phone_view_count', 'set_phone_view_count');
add_action('wp_ajax_nopriv_set_phone_view_count', 'set_phone_view_count');

function set_phone_view_count(){
	$project_id = $_POST['project_id'];

	$old_count = get_post_meta($project_id, 'get_phone', true);
	update_post_meta($project_id, 'get_phone', $old_count + 1);
}

//CRON
add_filter( 'cron_schedules', function($all_intervals){
	$all_intervals['every_4_min'] = array(
		'interval' => 240,
		'display' => 'Каждые 4 минуты'
	);

	$all_intervals['every_10_sec'] = array(
		'interval' => 10,
		'display' => 'Каждые 10 сек'
	);

	return $all_intervals;
});


//cron update register users count
if(!wp_next_scheduled('update_register_users_count_action')){
	wp_schedule_event(time(), 'every_4_min', 'update_register_users_count_action');
}

add_action('update_register_users_count_action', function(){

	$home_page_id = get_option('page_on_front');

	$register_users = get_post_meta($home_page_id, 'register_users_count', true);

	$new_register_users_count = intval($register_users) + random_int(1, 5);
	update_post_meta($home_page_id, 'register_users_count', $new_register_users_count);
});



//update register users count
add_action('wp_ajax_get_register_user_count', 'get_register_user_count');
add_action('wp_ajax_nopriv_get_register_user_count', 'get_register_user_count');
function get_register_user_count(){
	$home_page_id = get_option('page_on_front');

	$real_users_count = Users_Actions::get_all_users_count();
	$register_users = get_post_meta($home_page_id, 'register_users_count', true);

	echo json_encode(array(
		'register_users' => intval($register_users) + intval($real_users_count),
	));

	wp_die();
}

//cron update online users count
if(!wp_next_scheduled('update_online_users_count_action')){
	wp_schedule_event(time(), 'every_10_sec', 'update_online_users_count_action');
}

add_action('update_online_users_count_action', function(){
	$current_hours = intval(current_time('G')) + 1;

	$home_page_id = get_option('page_on_front');
	$online_users = intval(get_post_meta($home_page_id, 'online_users_count', true));
	$new_users_online_count = 0;

	$register_users = get_post_meta($home_page_id, 'register_users_count', true);

	if($current_hours >= 23){

		$new_users_online_count = ( $online_users > 300 ) ? $online_users + random_int(-3, 0) : $online_users + random_int(-2, 2);
		$new_users_online_count = ( $online_users < 100 ) ? $online_users + random_int(0, 3) : $new_users_online_count;

	}else if($current_hours >= 21){

		$new_users_online_count = ( $online_users > 600 ) ? $online_users + random_int(-10, 0) : $online_users + random_int(-20, 20);
		$new_users_online_count = ( $online_users < 100 ) ? $online_users + random_int(0, 10) : $new_users_online_count;

	}else if($current_hours >= 18){

		$new_users_online_count = ( $online_users > ( (intval($register_users) * 3 ) / 4) ) ? $online_users + random_int(-3, 0) : $online_users + random_int(-20, 20);
		$new_users_online_count = ( $online_users < 600 ) ? $online_users + random_int(10, 30) : $new_users_online_count;

	}else if($current_hours >= 12){

		$new_users_online_count = ( $online_users > ( (intval($register_users) * 3 ) / 4) ) ? $online_users + random_int(-20, -10) : $online_users + random_int(-20, 20);
		$new_users_online_count = ( $online_users < 600 ) ? $online_users + random_int(10, 30) : $new_users_online_count;

	}else if($current_hours >= 8){

		$new_users_online_count = ( $online_users > 600 ) ? $online_users + random_int(-10, 0) : $online_users + random_int(-20, 20);
		$new_users_online_count = ( $online_users < 100 ) ? $online_users + random_int(0, 10) : $new_users_online_count;

	}else if($current_hours >= 4){

		$new_users_online_count = ( $online_users > 300 ) ? $online_users + random_int(-3, 0) : $online_users + random_int(-2, 2);
		$new_users_online_count = ( $online_users < 100 ) ? $online_users + random_int(0, 3) : $new_users_online_count;

	}else if($current_hours >= 0){

		$new_users_online_count = ( $online_users > 200 ) ? $online_users + random_int(-3, 0) : $online_users + random_int(-2, 2);
		$new_users_online_count = ( $online_users < 70 ) ? $online_users + random_int(0, 3) : $new_users_online_count;

	}

	update_post_meta($home_page_id, 'online_users_count', $new_users_online_count);
});

//update online users count
add_action('wp_ajax_get_users_online_count', 'get_users_online_count');
add_action('wp_ajax_nopriv_get_users_online_count', 'get_users_online_count');
function get_users_online_count(){
	$home_page_id = get_option('page_on_front');

	$real_online_users_count = Users_Actions::get_online_count();
	$online_counter = get_post_meta($home_page_id, 'online_users_count', true);

	echo json_encode(array(
		'online_users' => intval($online_counter) + intval($real_online_users_count)
	));

	wp_die();
}