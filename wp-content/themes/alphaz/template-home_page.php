<?php
/**
 *template name: Home
 **/
?>
<?php get_header(); ?>

<?php do_action('get_slider', $post->ID); ?>

<section class="p-0">
    <div class="container-fluid">
	<?php if(have_posts()): ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	<?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>
