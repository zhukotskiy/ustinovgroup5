<?php
/**
 *template name: User Favorite
 **/
?>
<?php
get_header();
?>
<section class="light pt-5">
	<div class="container">
		<div class="row">
			<div class="col">
				<h5 class="text-right font-type-2">Рады снова вас видеть<?php do_action('get_user_name'); ?></h5>
			</div>
		</div>
		<div class="row">
			<div class="col"><h3>Профиль</h3></div>
			<!--            <div class="col">-->
			<!--                <h3 class="text-right font-type-2 mb-0">$600</h3>-->
			<!--                <h6 class="text-right text-caption pt-0 mt-0 font-type-2">на счету</h6>-->
			<!--            </div>-->
		</div>
		<div class="row">
			<div class="col-12 navigation d-flex flex-wrap">
				<a href="<?php echo site_url().'/profile/user-projects'?>" class="tab-item">
					<span>Мои проекты</span>
				</a>
				<a href="<?php do_action('get_user_messages_url') ?>" class="tab-item <?php echo (bp_is_user_messages()) ? 'active' : '' ;?>">
					<span>Сообщения</span>
				</a>
				<a href="<?php echo site_url().'/profile/favorite'?>" class="tab-item active">
					<span>Избранное</span>
				</a>
				<a href="<?php do_action('get_user_profile_settings_url') ?>" class="tab-item <?php echo (bp_is_user_settings()) ? 'active' : '' ;?>">
					<span>Настройки аккаунта</span>
				</a>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="divider"></div>
		</div>
	</div>
</section>
	<section id="user-projects" class="light">
		<div class="container">
			<div class="row">
				<?php $user_favorite_items = get_user_favorites(wp_get_current_user()->ID); ?>
				<?php if(!empty($user_favorite_items)): ?>
					<?php foreach($user_favorite_items as $post_id): ?>
						<?php $post = get_post($post_id); ?>
						<?php include( locate_template( 'parts/loop/project-loop-slider.php')); ?>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="col-12">
						<h3 class="text-center text-secondary">Помеченых проектов не найдено</h3>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>


<!-- Footer -->
<?php get_footer(); ?>
