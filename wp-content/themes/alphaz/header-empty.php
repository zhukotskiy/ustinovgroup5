<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>USTINN</title>
    <meta name="description" content="USTINN - самая крупная онлайн бизнес платформа объявлений Украины. Огромная база предложений по темам: готовый бизнес, стартап, кредитование, франшиза, тендеры, краудфандинг. Вы можете найти инвестора в Украине для своего бизнеса или вложить деньги в интересные проекты.">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/loader.css'?>">
	<?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117937815-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117937815-1');
    </script>

</head>
<body>

