<?php
    wp_enqueue_style('slick-css');
    wp_enqueue_style('lightgallery-css');
    wp_enqueue_script('jquery_mousewheel-js');
    wp_enqueue_script('lightgallery-js');
    wp_enqueue_script('picturefill-js');

    get_header();
?>
<?php $project_type = get_post_meta($post->ID, 'project_type', true); ?>
<div id="single-project-page">
    <section class="light mt-5 pt-2">
        <div class="container">
            <div class="row d-none d-sm-flex">
                <div class="col-12 breadcrumbs">
                    <a href="<?php echo get_site_url(). '/business'?>">Главная</a>
                    <span class="lnr lnr-chevron-right"></span>
                    <a href="javascript:void(0)" class="disabled">Проекты</a>
                    <span class="lnr lnr-chevron-right"></span>
                    <?php $cur_project_type = wp_get_post_terms($post->ID, 'projects_type', array('fields' => 'all'))[0]; ?>
                    <a href="<?php echo get_site_url(). '/projects_type/'. $cur_project_type->slug; ?>"><?php echo $cur_project_type->name; ?></a>
                    <span class="lnr lnr-chevron-right"></span>
                    <a href="javascript:void(0)" class="disabled"><?php echo $post->post_title; ?></a>
                </div>
            </div>
            <div class="row mt-3 mt-sm-0">
                <div class="col-lg-6 gallery order-lg-1 order-2 mt-5 mt-lg-0">
                    <?php $gallery = get_gallery($post->ID); ?>
	                <?php echo get_favorites_button($post->ID);?>
                    <div id="project-page-slide">
                        <?php if(count($gallery) > 0 && $gallery): ?>
                            <?php foreach($gallery as $item): ?>
                                <a href="<?php echo $item->large_url ?>">
                                    <img src="<?php echo $item->large_url ?>">
                                </a>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <a href="<?php echo get_thumbnail($post->ID); ?>">
                                <img src="<?php echo get_thumbnail($post->ID); ?>">
                            </a>
                        <?php endif; ?>
                    </div>
                    <?php $gallery = get_gallery($post->ID); ?>
                        <?php if (!empty($gallery) && $gallery): ?>
                            <div id="project-page-slide-nav" class="mt-3">
                                <?php foreach($gallery as $item): ?>
                                    <div class="slide-item">
                                        <img src="<?php echo $item->large_url ?>">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                    <?php endif; ?>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <div class="project-title"><?php echo $post->post_title; ?></div>
                    <div class="secondary-info">
                        <span>Номер проекта:</span>
                        <span><?php echo get_post_meta($post->ID, 'sku', true); ?></span>
                    </div>

	                <?php $cur_project_type = wp_get_post_terms($post->ID, 'projects_type', array('fields' => 'all'))[0]; ?>
	                <?php if($cur_project_type->name == 'Краудфандинг'): ?>
                        <?php
                            $invest_sum = get_post_meta($post->ID, 'invest_sum', true);
                            $invest_already = get_post_meta($post->ID, 'invest_already', true);
                            $investors_count = get_post_meta($post->ID, 'investors_count', true);

                            $progress = 0;
                            if(!empty($invest_already) && !empty($invest_sum)){
                                $progress = intval((100 * intval(preg_replace("/[^,.0-9]/", '', $invest_already))) / intval(preg_replace("/[^,.0-9]/", '', $invest_sum)) );
                            }
                        ?>
                        <div class="project-price mt-4"><?php echo $invest_already; ?></div>
                        <div class="progress-info">
                            <div class="progress-bar">
                                <div class="progress" style="width: <?php echo $progress. '%'; ?>"></div>
                            </div>
                            <div class="invest-already-info">
                                <span>собрано из </span>
                                <span><?php echo $invest_sum; ?></span>
                            </div>
                        </div>
                    <?php else: ?>

		                <?php $price = get_post_meta($post->ID, 'invest_sum', true); ?>
                        
                        <?php $currency_val = ''; ?>
                        <?php if($price != 'Договорная' && !empty($price) && $price != 0): ?>
                            <?php $prepended_price = preg_replace('~\D+~', '', $price); ?>
                            <?php $formated_price = number_format($prepended_price, 0, ',', ' '); ?>
                            <?php $currency = get_post_meta($post->ID, 'currency', true); ?>
                            <?php
                                if($currency == 'usd' || empty($currency)){
                                    $currency_val = ' $';

                                }else if($currency == 'uah'){
                                    $currency_val = ' грн';

                                }else if($currency == 'eur'){
                                    $currency_val = ' €';

                                }
                            ?>
                            <div class="project-price mt-4"><?php echo $formated_price. $currency_val; ?></div>
                        <?php else: ?>
                            <div class="project-price mt-4">Договорная</div>
                        <?php endif; ?>

                    <?php endif;?>


                    <p class="mt-4"><?php echo get_post_meta($post->ID, 'short_description', true); ?></p>

	                <?php $cur_project_type = wp_get_post_terms($post->ID, 'projects_type', array('fields' => 'all'))[0]; ?>

                    <?php $phone = get_post_meta($post->ID, 'phone', true); ?>
                    <?php if(!empty($phone)): ?>
                        <div class="d-flex mt-4">
                            <span class="show-contacts filled-btn--small" data-project_id="<?php echo $post->ID; ?>">Показать контакты</span>
                        </div>
                        <div id="contacts-info">
                            <div class="contacts-info-item mt-3">
                                <span class="lnr lnr-phone-handset mr-1"></span>
                                <span><?php echo $phone; ?></span>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="main-info mt-4">
	                    <?php $cur_project_type = wp_get_post_terms($post->ID, 'projects_type', array('fields' => 'all'))[0]; ?>
		                <?php if(!empty($cur_project_type)): ?>
                            <h5 class="main-info-item m-0">
                                <span class="name">Тип проекта:</span>
                                <span class="text-grey"><?php echo $cur_project_type->name; ?></span>
                            </h5>
		                <?php endif; ?>

	                    <?php $cur_project_cat = wp_get_post_terms($post->ID, 'projects_cat', array('fields' => 'all'))[0]; ?>
	                    <?php if(!empty($cur_project_cat)): ?>
                            <h5 class="main-info-item m-0">
                                <span class="name">Категория:</span>
                                <span class="text-grey"><?php echo $cur_project_cat->name; ?></span>
                            </h5>
	                    <?php endif; ?>

		                <?php $project_address = get_post_meta($post->ID, 'address', true); ?>
		                <?php if($project_address): ?>
                            <h5 class="main-info-item m-0">
                                <span class="name">Местоположение:</span>
                                <span class="text-grey"><?php echo $project_address; ?></span>
                            </h5>
		                <?php endif; ?>

		                <?php $project_link = get_post_meta($post->ID, 'project_link', true); ?>
		                <?php if(!empty($project_link)): ?>
                            <div class="main-info-item ">
                                <span class="name text-caption">Сайт:</span>
                                <a href="<?php echo $project_link; ?>" target="_blank"><?php echo $project_link; ?></a>
                            </div>
		                <?php endif; ?>
                    </div>

                    <div class="additional-info mt-4">
	                    <?php $cur_project_type = wp_get_post_terms($post->ID, 'projects_type', array('fields' => 'all'))[0]; ?>
	                    <?php if($cur_project_type->name == 'Краудфандинг'): ?>
                            <?php $investors_count = get_post_meta($post->ID, 'investors_count', true); ?>
                            <span class="additional-info-item">
                                <span class="mr-2 lnr lnr-users"></span>
                                <span class="mr-2"><?php echo (!empty($investors_count)) ? $investors_count : 0; ?></span>
                            </span>
                        <?php endif; ?>

                        <?php $project_favorite_count = strip_tags(get_favorites_count($post->ID)); ?>
                        <?php $def_favorite_count = get_post_meta($post->ID, 'add_to_favorite', true); ?>
                        <span class="additional-info-item">
                            <span class="mr-2 lnr lnr-star"></span>
                            <span class="mr-2"><?php echo intval($project_favorite_count) + intval($def_favorite_count); ?></span>
                        </span>
		                <?php $project_views_count = get_post_meta($post->ID, 'views', true); ?>
		                <?php update_post_meta($post->ID, 'views', intval($project_views_count) + 1);?>
                        <span class="additional-info-item">
                            <span class="mr-2 lnr lnr-eye"></span>
                            <span class="mr-2"><?php echo $project_views_count; ?></span>
                        </span>

		                <?php $project_interested_count = get_post_meta($post->ID, 'get_phone', true); ?>
                        <span class="additional-info-item">
                            <span class="mr-2 lnr lnr-phone-handset"></span>
                            <span class="mr-2"><?php echo (!empty($project_interested_count)) ? $project_interested_count : '0'; ?></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="light">
        <div class="divider"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="description pt-4">
		                <?php while ( have_posts() ) : the_post(); ?>
			                <?php the_content(); ?>
		                <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" class="row send-message">
                <input type="hidden" name="action" value="send_message_to_author">
                <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="row">
                        <div class="col-12 mt-5">
                            <label for="message_content">Написать владельцу</label>
                            <input type="hidden" id="message_content" name="message-content" data-type="materialTextArea">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end justify-content-md-start mt-4">
                            <button class="filled-btn--small">Отправить</button>

                        </div>
                    </div>
                </div>
            </form>
        </div>

    </section>
    <section class="light">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-left">Похожие проекты</h1>
                </div>
            </div>

            <div class="row">
		        <?php do_action('get_related_posts', $post->ID); ?>
            </div>
        </div>
    </section>

</div>



<?php
    wp_enqueue_script('slick-slider-js');
    get_footer();
?>
